package com.appetiser.module.local

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.appetiser.module.local.features.AppDatabase
import com.appetiser.module.local.features.movie.MovieLocalSource
import com.appetiser.module.local.features.movie.MovieLocalSourceImpl
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.local.features.session.SessionLocalSourceImpl
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.token.AccessTokenLocalSourceImpl
import com.appetiser.module.local.features.user.UserLocalSource
import com.appetiser.module.local.features.user.UserLocalSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application.applicationContext)
    }

    @Provides
    @Singleton
    fun providesUserLocalSource(appDatabase: AppDatabase): UserLocalSource {
        return UserLocalSourceImpl(appDatabase.userDao())
    }

    @Provides
    @Singleton
    fun providesMovieLocalSource(
        appDatabase: AppDatabase,
        sharedPreferences: SharedPreferences
    ): MovieLocalSource {
        return MovieLocalSourceImpl(
            appDatabase.movieDao(),
            sharedPreferences
        )
    }

    @Provides
    @Singleton
    fun providesAccessTokenLocalSource(
        sharedPreferences: SharedPreferences,
        appDatabase: AppDatabase
    ): AccessTokenLocalSource {
        return AccessTokenLocalSourceImpl(
            sharedPreferences,
            appDatabase.tokenDao()
        )
    }

    @Provides
    @Singleton
    fun providesSessionLocalSource(
        userLocalSource: UserLocalSource,
        accessTokenLocalSource: AccessTokenLocalSource,
        sharedPreferences: SharedPreferences
    ): SessionLocalSource {
        return SessionLocalSourceImpl(
            userLocalSource,
            accessTokenLocalSource,
            sharedPreferences
        )
    }
}
