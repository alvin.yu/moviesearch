package com.appetiser.module.local.features.token

import com.appetiser.module.domain.models.token.AccessToken
import io.reactivex.Completable
import io.reactivex.Single

interface AccessTokenLocalSource {
    fun getAccessToken(): Single<AccessToken>

    fun getAccessTokenFromPref(): String

    fun saveAccessToken(accessToken: AccessToken): Single<AccessToken>

    fun deleteToken(): Completable
}
