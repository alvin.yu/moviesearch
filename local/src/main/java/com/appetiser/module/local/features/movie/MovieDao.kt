package com.appetiser.module.local.features.movie

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
abstract class MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveMovieList(movieList: List<MovieDB>): Single<List<Long>>

    @Query("SELECT * FROM movie_table")
    abstract fun getAllMovies(): Flowable<List<MovieDB>>

    @Query("SELECT * FROM movie_table WHERE trackId = :trackId")
    abstract fun getMovieById(trackId: Long): Single<MovieDB>
}
