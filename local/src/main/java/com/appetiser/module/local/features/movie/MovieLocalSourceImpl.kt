package com.appetiser.module.local.features.movie

import android.content.SharedPreferences
import com.appetiser.module.domain.models.movie.Movie
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class MovieLocalSourceImpl @Inject constructor(
    private val movieDao: MovieDao,
    private val sharedPreferences: SharedPreferences
) : MovieLocalSource {

    override fun saveMovieList(movieList: List<Movie>): Single<List<Long>> {
        return movieDao.saveMovieList(movieList.map { movie ->
            MovieDB.fromDomain(movie)
        })
    }

    override fun getAllMovies(): Flowable<List<Movie>> {
        return movieDao.getAllMovies().map { movieDBList ->
            movieDBList.map { movieDB ->
                MovieDB.toDomain(movieDB)
            }
        }
    }

    override fun getMovieById(trackId: Long): Single<Movie> {
        return movieDao.getMovieById(trackId).map {
            MovieDB.toDomain(it)
        }
    }

    override fun getLastUserVisit(): Single<Long> {
        return Single.just(
            sharedPreferences
                .getLong(
                    PREF_LAST_USER_VISIT,
                    PREF_LAST_USER_VISIT_DEFAULT_VALUE
                )
        )
    }

    companion object {
        const val PREF_LAST_USER_VISIT_DEFAULT_VALUE = 0L
        const val PREF_LAST_USER_VISIT = "PREF_LAST_USER_VISIT"
    }

}
