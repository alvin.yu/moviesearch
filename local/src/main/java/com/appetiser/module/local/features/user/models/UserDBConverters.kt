package com.appetiser.module.local.features.user.models

import androidx.room.TypeConverter
import com.appetiser.module.domain.models.user.MediaFile
import com.google.gson.Gson

import java.time.Instant

class UserDBConverters {
    @TypeConverter
    fun avatarFromString(value: String?): MediaFile {
        return Gson().fromJson(value ?: "{}", MediaFile::class.java)
    }

    @TypeConverter
    fun avatarToString(avatar: MediaFile?): String {
        return Gson().toJson(avatar ?: MediaFile.empty())
    }

    @TypeConverter
    fun fromInstant(value: Instant): Long {
        return value.toEpochMilli()
    }

    @TypeConverter
    fun toInstant(value: Long): Instant {
        return Instant.ofEpochMilli(value)
    }
}
