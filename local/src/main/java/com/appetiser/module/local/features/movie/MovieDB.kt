package com.appetiser.module.local.features.movie

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.models.movie.Movie


@Entity(tableName = "movie_table")
data class MovieDB(
    @PrimaryKey val trackId         : Long,
    @ColumnInfo val trackName       : String,
    @ColumnInfo val artworkUrl30    : String,
    @ColumnInfo val artworkUrl100   : String,
    @ColumnInfo val trackPrice      : Double = 0.0,
    @ColumnInfo val primaryGenreName: String,
    @ColumnInfo val longDescription : String,
) {
    companion object {

        fun fromDomain(movie: Movie): MovieDB {
            return with(movie) {
                MovieDB(
                    trackId,
                    trackName,
                    artworkUrl30,
                    artworkUrl100,
                    trackPrice,
                    primaryGenreName,
                    longDescription
                )
            }
        }

        fun toDomain(movieDB: MovieDB): Movie {
            return with(movieDB) {
                Movie(
                    trackId,
                    trackName,
                    artworkUrl30,
                    artworkUrl100,
                    trackPrice,
                    primaryGenreName,
                    longDescription
                )
            }
        }


    }

}