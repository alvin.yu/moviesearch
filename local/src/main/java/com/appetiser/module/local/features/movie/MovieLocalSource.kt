package com.appetiser.module.local.features.movie

import com.appetiser.module.domain.models.movie.Movie
import io.reactivex.Flowable
import io.reactivex.Single

interface MovieLocalSource {

    fun saveMovieList(movieList: List<Movie>): Single<List<Long>>

    fun getAllMovies(): Flowable<List<Movie>>

    fun getMovieById(trackId: Long): Single<Movie>

    fun getLastUserVisit(): Single<Long>
}
