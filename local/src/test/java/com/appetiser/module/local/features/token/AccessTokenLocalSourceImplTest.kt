package com.appetiser.module.local.features.token

import android.content.SharedPreferences
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.local.features.Stubs
import com.appetiser.module.local.features.token.dao.TokenDao
import com.appetiser.module.local.features.token.models.AccessTokenDB
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class AccessTokenLocalSourceImplTest {

    private val sharedPreferences: SharedPreferences = mock()
    private val tokenDao: TokenDao = mock()

    private lateinit var subject: AccessTokenLocalSource

    @Before
    fun setUp() {
        subject = AccessTokenLocalSourceImpl(sharedPreferences, tokenDao)
    }

    @Test
    fun getAccessToken_ShouldMapDbTokenToDomainAccessToken() {
        val token = Stubs.TOKEN
        val expected = AccessToken(token = token)

        Mockito.`when`(tokenDao.getToken()).thenReturn(Single.just(AccessTokenDB(token = token)))

        subject
            .getAccessToken()
            .test()
            .assertComplete()
            .assertValue { it == expected }
    }
}
