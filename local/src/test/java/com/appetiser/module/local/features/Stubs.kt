package com.appetiser.module.local.features

import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.local.features.user.models.UserDB
import java.time.Instant

object Stubs {
    val DB_USER_SESSION = UserDB(
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        uid = "1231232132123"
    )

    val EMPTY_DB_USER_SESSION = UserDB.empty()

    val USER_SESSION = User(
        id = "1231232132123",
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "12/28/1993",
        updatedAt = Instant.now(),
        createdAt = Instant.now(),
        blockedAt = Instant.now(),
        onBoardedAt = Instant.now()
    )

    val TOKEN = "12321312321321321312"

    val SESSION = Session(
        USER_SESSION,
        AccessToken(token = TOKEN)
    )
}
