package com.appetiser.module.local.features.session

import android.content.SharedPreferences
import com.appetiser.module.domain.utils.anyString
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.user.UserLocalSource
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.Mockito

class SessionLocalSourceImplTest {

    private val userLocalSource: UserLocalSource = mock()
    private val accessTokenLocalSource: AccessTokenLocalSource = mock()
    private val sharedPreferences: SharedPreferences = mock()
    private val sharedPreferencesEditor: SharedPreferences.Editor = mock()

    private lateinit var subject: SessionLocalSource

    @Before
    fun setUp() {
        subject = SessionLocalSourceImpl(
            userLocalSource,
            accessTokenLocalSource,
            sharedPreferences
        )
    }

    @Test
    fun logout_ShouldLogoutUserAndToken() {
        whenever(userLocalSource.deleteUser())
            .thenReturn(Completable.complete())
        whenever(accessTokenLocalSource.deleteToken())
            .thenReturn(Completable.complete())
        whenever(sharedPreferences.edit())
            .thenReturn(sharedPreferencesEditor)
        whenever(sharedPreferencesEditor.putBoolean(anyString(), anyBoolean()))
            .thenReturn(sharedPreferencesEditor)

        subject
            .clearSession()
            .test()
            .assertComplete()

        Mockito
            .verify(
                userLocalSource,
                Mockito.times(1)
            )
            .deleteUser()
        Mockito
            .verify(
                accessTokenLocalSource,
                Mockito.times(1)
            )
            .deleteToken()
    }
}
