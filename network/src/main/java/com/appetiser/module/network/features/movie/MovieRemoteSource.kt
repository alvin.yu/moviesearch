package com.appetiser.module.network.features.movie

import com.appetiser.module.domain.models.movie.Movie
import io.reactivex.Single

interface MovieRemoteSource {

    fun getMovies(term: String, country: String, media: String, limit: String): Single<List<Movie>>

}