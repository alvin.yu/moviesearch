package com.appetiser.module.network.features.movie

import com.appetiser.module.domain.models.movie.Movie
import com.appetiser.module.network.base.response.BaseResponse

data class MovieListResponse (
    val results: List<Movie>
) : BaseResponse()