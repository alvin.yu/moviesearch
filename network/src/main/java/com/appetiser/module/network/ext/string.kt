package com.appetiser.module.network.ext

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

fun String.fromIsoDateTimeToInstant(): Instant {
    return LocalDateTime.parse(this, DateTimeFormatter.ISO_DATE_TIME)
        .atZone(ZoneId.systemDefault())
        .toInstant()
}
