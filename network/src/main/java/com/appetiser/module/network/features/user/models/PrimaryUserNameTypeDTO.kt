package com.appetiser.module.network.features.user.models

import com.appetiser.module.domain.models.user.PrimaryUserNameType
import com.google.gson.annotations.SerializedName

enum class PrimaryUserNameTypeDTO(val value: String) {
    @SerializedName("email")
    EMAIL("email"),

    @SerializedName("phone_number")
    PHONE_NUMBER("phone_number");

    companion object {
        fun toDomain(item: PrimaryUserNameTypeDTO?): PrimaryUserNameType {
            return if (item == null) {
                PrimaryUserNameType.EMAIL
            } else {
                if (item == EMAIL) PrimaryUserNameType.EMAIL else PrimaryUserNameType.PHONE_NUMBER
            }
        }
    }
}
