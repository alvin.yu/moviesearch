package com.appetiser.module.network.base.response

open class BaseResponse(
    val success: Boolean = false,
    val message: String = "",
    val http_status: Int = 500
) {

    fun isSuccessful(): Boolean = http_status == 200 || http_status == 201
}
