package com.appetiser.module.network.features.movie

import com.appetiser.module.domain.models.movie.Movie
import com.appetiser.module.network.base.BaseRemoteSource
import com.google.gson.Gson
import io.reactivex.Single
import javax.inject.Inject

class MovieRemoteSourceImpl @Inject constructor(
    private val apiServices: MovieApiServices,
    private val gson: Gson
) : BaseRemoteSource(), MovieRemoteSource {

    override fun getMovies(term: String, country: String, media: String, limit: String): Single<List<Movie>> {
        return apiServices.getMovies(term, country, media, limit).map {
            it.results
        }
    }

}