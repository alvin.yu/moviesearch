package com.appetiser.module.network.base.response

import com.google.gson.annotations.SerializedName

data class BaseErrorResponse<T>(
    @SerializedName("meta") val meta: T,
    @SerializedName("message") val message: String,
    @SerializedName("http_status") val httpStatusCode: Int,
    @SerializedName("success") val success: Boolean,
    @SerializedName("error_code") val errorCode: String
)
