package com.appetiser.module.network

const val BASE_URL = "https://itunes.apple.com/"
const val API = "api"
const val VERSION = "v1"
const val ENDPOINT_FORMAT = "%s/%s/%s/"

const val HTTP_UNPROCESSABLE_ENTITY = 422
