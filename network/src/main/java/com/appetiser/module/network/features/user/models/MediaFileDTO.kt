package com.appetiser.module.network.features.user.models

import com.appetiser.module.domain.models.user.MediaFile
import com.google.gson.annotations.SerializedName

data class MediaFileDTO(
    @SerializedName("id") val id: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("file_name") val fileName: String? = "",
    @SerializedName("collection_name") val collectionName: String? = "",
    @SerializedName("mime_type") val mimeType: String? = "",
    @SerializedName("size") val size: Long? = 0,
    @SerializedName("created_at") val createdAt: String? = "",
    @SerializedName("url") val url: String? = "",
    @SerializedName("thumb_url") val thumbUrl: String? = ""
) {
    companion object {
        fun empty(): MediaFileDTO {
            return MediaFileDTO(
                id = "",
                name = "",
                fileName = "",
                collectionName = "",
                mimeType = "",
                size = 0,
                createdAt = "",
                url = "",
                thumbUrl = ""
            )
        }

        fun toDomain(mediaFileDTO: MediaFileDTO?): MediaFile {
            return if (mediaFileDTO == null) MediaFile.empty() else with(mediaFileDTO) {
                MediaFile(
                    id = id.orEmpty(),
                    name = name.orEmpty(),
                    fileName = fileName.orEmpty(),
                    collectionName = collectionName.orEmpty(),
                    mimeType = mimeType.orEmpty(),
                    size = size ?: 0,
                    createdAt = createdAt.orEmpty(),
                    url = url.orEmpty(),
                    thumbUrl = thumbUrl.orEmpty()
                )
            }
        }
    }
}
