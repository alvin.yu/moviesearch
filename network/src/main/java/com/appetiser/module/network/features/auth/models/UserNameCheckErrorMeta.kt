package com.appetiser.module.network.features.auth.models

import com.google.gson.annotations.SerializedName

data class UserNameCheckErrorMeta(
    @SerializedName("username_type") val usernameType: String,
    @SerializedName("auth_type") val authType: String
)
