package com.appetiser.module.network.features.movie

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

private const val TERM    = "term"
private const val COUNTRY = "country"
private const val MEDIA   = "media"
private const val LIMIT   = "limit"

interface MovieApiServices {

    @GET("search")
    fun getMovies(
        @Query(TERM   ) term   : String,
        @Query(COUNTRY) country: String,
        @Query(MEDIA  ) media  : String,
        @Query(LIMIT  ) limit  : String,
    ): Single<MovieListResponse>

}