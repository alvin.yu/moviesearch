package com.appetiser.module.network.base.response

import com.google.gson.annotations.SerializedName

data class PagingMeta(
    @SerializedName("current_page") val currentPage: Int,
    val from: Int? = 0,
    @SerializedName("last_page") val lastPage: Int,
    val path: String,
    @SerializedName("per_page") val perPage: Int,
    val to: Int,
    val total: Int
)
