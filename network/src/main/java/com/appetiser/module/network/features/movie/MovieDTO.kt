package com.appetiser.module.network.features.movie

import com.appetiser.module.domain.models.movie.Movie
import com.google.gson.annotations.SerializedName

open class MovieDTO(
    @field:SerializedName("track_id")           val trackId         : Long?   = 0L,
    @field:SerializedName("track_name")         val trackName       : String? = "",
    @field:SerializedName("artwork_url30")      val artworkUrl30    : String? = "",
    @field:SerializedName("artwork_url100")     val artworkUrl100   : String? = "",
    @field:SerializedName("track_price")        val trackPrice      : Double? = 0.0,
    @field:SerializedName("primary_genre_name") val primaryGenreName: String? = "",
    @field:SerializedName("long_description")   val longDescription : String? = "",
) {
    companion object {
        fun toDomain(movieDTO: MovieDTO?): Movie {
            return if (movieDTO == null) {
                Movie.empty()
            } else {
                with(movieDTO) {
                    Movie(
                        trackId ?: 0L,
                        trackName.orEmpty(),
                        artworkUrl30.orEmpty(),
                        artworkUrl100.orEmpty(),
                        trackPrice ?: 0.0,
                        primaryGenreName.orEmpty(),
                        longDescription.orEmpty()
                    )
                }
            }
        }
    }
}