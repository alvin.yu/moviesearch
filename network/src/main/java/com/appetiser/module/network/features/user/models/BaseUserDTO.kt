package com.appetiser.module.network.features.user.models

import com.google.gson.annotations.SerializedName

open class BaseUserDTO(
    @SerializedName("last_name") open val lastName: String? = "",
    @SerializedName("first_name") open val firstName: String? = "",
    @SerializedName("full_name") open val fullName: String? = "",
    @SerializedName("avatar_permanent_url") open val avatarPermanentUrl: String? = "",
    @SerializedName("avatar_permanent_thumb_url") open val avatarPermanentThumbUrl: String? = "",
    open val id: String = "",
    @SerializedName("avatar") val avatar: MediaFileDTO? = MediaFileDTO.empty(),
    val description: String? = "",
    @SerializedName("birthdate") val birthDate: String? = "",
    @SerializedName("created_at")
    open var createdAt: String? = null,
    @SerializedName("updated_at")
    open var updatedAt: String? = null,
    @SerializedName("blocked_at")
    open var blockedAt: String? = null
)
