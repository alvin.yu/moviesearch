These are my knowledge gaps during the technical_interview coding_challenge with Francis. \
I learned a lot from Francis and look forward to further mentorship from everyone.

Please click the item to expand the section:

<details><summary>0. SlidingPaneLayout</summary>

The UI design is the same but the View used is SlidingPaneLayout instead of Master-Detail used during the interview. \
In SlidingPaneLayout, the user can slide the detail pane like a NavigationDrawer. \
There are quirks that isn't handled like the back button on the detail pane and the back stack.

The HomeFragment is where the movie list is displayed: \
[Link to fragment_home SlidingPaneLayout](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/res/layout/fragment_home.xml#L22).

**Portrait Movie List** \
![Portrait Movie List](blob/Portrait - Movie List.png)

**Portrait Movie Detail** \
![Portrait Movie Detail](blob/Portrait - Movie Detail.png)

**Portrait Sliding Detail Pane Halfway** \
![Portrait Sliding Detail Pane Halfway](blob/Portrait - Sliding Detail Pane Halfway.png)

**Landscape Movie List with No Selected Movie** \
![Landscape Movie List with No Selected Movie](blob/Landscape - Movie List with No Selected Movie.png)

**Landscape Movie List with a Selected Movie** \
![Landscape Movie List with a Selected Movie](blob/Landscape - Movie List with a Selected Movie.png)

</details>

<details><summary>1. Base (Inheritance)</summary>

There were a lot of similar codes between my Activities, Fragments, ViewModels, Factory, etc. which breaks DRY. \
The BasePlate takes away a lot of those boilerplate codes which I applied on the following classes: 
1. [HomeFragment and MovieDetailFragment inherits BaseViewModelFragment which inherits BaseFragment](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/features/home/HomeFragment.kt#L18)
2. [MovieViewModel inherits BaseViewModel](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/features/home/MovieViewModel.kt) 
3. MovieListAdapter could inherit BaseListAdapter but skipped for now because I couldn't figure out inheriting BaseListAdapter with different ViewHolders for the RecyclerView header and the list items.
4. [I also learned from the BasePlate that I don't need to make a ViewModelFactory for every ViewModel.](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/ViewModelFactory.kt)

</details>

<details><summary>2. Dagger2</summary>

I was manually implementing Dependency Injection which caused a lot of boilerplate code. \
Dagger removes clutter by automating DI through annotations and checks correctness during compile-time. \
For example, when creating Fragments, it should be included in [FragmentBuilder](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/di/builders/FragmentBuilder.kt#L147) so that DI knows to inject its ViewDataBinding and ViewModels.

I implemented Dagger2 from scratch in another project that wasn't cloned from BasePlate to learn the basics:
1. [@Providing Application with a @Singleton scope](https://gitlab.com/alvin.yu/iSearch/-/blob/master/app/src/main/java/au/com/appetiser/isearch/di/AppModule.kt)
2. [Telling Dagger2 of the components and to inject the MovieListActivity](https://gitlab.com/alvin.yu/iSearch/-/blob/master/app/src/main/java/au/com/appetiser/isearch/di/AppComponent.kt)
3. [Providing Repository to MovieListViewModel](https://gitlab.com/alvin.yu/iSearch/-/blob/master/app/src/main/java/au/com/appetiser/isearch/di/StorageModule.kt)
4. [Providing Database to Repository](https://gitlab.com/alvin.yu/iSearch/-/blob/master/app/src/main/java/au/com/appetiser/isearch/di/DatabaseModule.kt)

There's still a lot to into in this space :)

</details>

<details><summary>3. DataBinding</summary>

I was inconsistently using DataBinding by mixing in the manual findViewById(...). \
I haven't figured how to access \<include\>s with similar names using DataBinding. \
For now, [with SlidingPaneLayout, DataBinding is used to access it.](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/features/home/HomeFragment.kt#L134)


</details>

<details><summary>4. Fragments</summary>

I was using multiple Activities where a single Activity with multiple Fragments could've worked for flexibility. \
So, now, we're using two (2) Fragments and hitching with the existing MainActivity.

PS. I should've created a separate package for movie instead of [putting everything in home](https://gitlab.com/alvin.yu/moviesearch/-/tree/master/app/src/main/java/com/appetiser/moviesearch/features/home).

</details>

<details><summary>5. GSon</summary>

For JSON parsing, I was using Moshi whereas the tech stack here uses Gson. \
Both are okay and as far as I know [can be used interchangeably like plug and play](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/network/src/main/java/com/appetiser/module/network/NetworkModule.kt#L69).

</details>

<details><summary>6. NavComponent + NavArgs</summary>

I ran out of time to implement Navigation Components and Safe Arguments in the interview coding_challenge. \
Here, a simple [navigation using the navController is done](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/features/home/HomeFragment.kt#L131). \
The [detail pane of the SlidingPaneLayout is the NavHostFragment](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/res/layout/fragment_home.xml#L51) that navigates [between Select A Movie screen - a movie isn't selected yet - and the Movie Detail screen - when a movie is selected.](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/res/navigation/movie_graph.xml)

</details>

<details><summary>7. RxJava2</summary>

I used LiveData+Coroutines, whereas the tech stack here uses RxJava.
We replaced Room+LiveData with [Room+Flowable to notify objects subscribed to changes in the database](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/local/src/main/java/com/appetiser/module/local/features/movie/MovieDao.kt#L17). Both essentially work the same wherein when we [load cached data](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/features/home/MovieViewModel.kt#L42) from database then display it, while [getting updated data from server](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/features/home/MovieViewModel.kt#L64) and [saving updated data to database](https://gitlab.com/alvin.yu/moviesearch/-/blob/master/app/src/main/java/com/appetiser/moviesearch/features/home/MovieViewModel.kt#L83): the movie list is automatically notified of the changes.

It's an interesting way to learn RxJava and look forward to learning more about schedulers, disposables, etc.

</details>
