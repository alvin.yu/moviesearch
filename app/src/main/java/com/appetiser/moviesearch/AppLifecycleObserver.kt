package com.appetiser.moviesearch

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.preference.PreferenceManager
import com.appetiser.module.local.features.movie.MovieLocalSourceImpl.Companion.PREF_LAST_USER_VISIT
import java.util.*

class AppLifecycleObserver(private val applicationContext: Context) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onBackground() {
        updateLastUserVisitTimestampInSharedPreferences()
    }

    private fun updateLastUserVisitTimestampInSharedPreferences() {
        val now = Date().time
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        with(sharedPref.edit()) {
            putLong(
                PREF_LAST_USER_VISIT,
                now
            )
            apply()
        }
    }
}
