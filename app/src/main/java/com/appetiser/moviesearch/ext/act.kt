package com.appetiser.moviesearch.ext

import android.app.Activity
import android.view.WindowManager

fun Activity.makeSoftInputModeAdjustPan() {
    window.apply {
        // If input mode is already adjustPan. Return.
        if (attributes.softInputMode == WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN) return

        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }
}

fun Activity.makeSoftInputModeAdjustResize() {
    window.apply {
        // If input mode is already adjustResize. Return.
        if (attributes.softInputMode == WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE) return

        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }
}
