package com.appetiser.moviesearch.ext

import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import androidx.annotation.DrawableRes
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.google.android.material.textfield.TextInputLayout

fun EditText.setDrawableEnd(@DrawableRes drawableId: Int) {
    setCompoundDrawablesWithIntrinsicBounds(
        0,
        0,
        drawableId,
        0
    )
}

fun EditText.indicatorIcons(@DrawableRes enabledIcon: Int, @DrawableRes disabled: Int) {
    addTextChangedListener(object : TextWatcher {

        override fun onTextChanged(input: CharSequence, start: Int, before: Int, count: Int) {
            if (input.isNotEmpty()) {
                setDrawableEnd(enabledIcon)
            } else {
                setDrawableEnd(disabled)
            }
        }

        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    })
}

fun TextInputLayout.setCustomPasswordTransformation() {
    setEndIconOnClickListener {
        editText?.let { et ->
            // Store the current cursor position
            val selection = et.selectionEnd

            if (et.transformationMethod != null &&
                et.transformationMethod is PasswordTransformationMethod
            ) {
                et.transformationMethod = null
            } else {
                et.transformationMethod = CustomPasswordTransformation.getInstance()
            }

            // And restore the cursor position
            if (selection >= 0) {
                et.setSelection(selection)
            }
        }
    }

    editText?.transformationMethod = CustomPasswordTransformation.getInstance()
}
