package com.appetiser.moviesearch.features.splash

sealed class SplashState {
    /**
     * User has logged in and completed onboarding flow.
     */
    object UserIsLoggedIn : SplashState()

    /**
     * User is not logged in, will show walkthrough.
     */
    object UserIsNotLoggedIn : SplashState()

    /**
     * User has logged in but has no name yet.
     */
    object NoFullName : SplashState()

    object NoProfilePhoto : SplashState()
}
