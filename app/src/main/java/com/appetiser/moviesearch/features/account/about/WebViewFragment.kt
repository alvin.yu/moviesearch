package com.appetiser.moviesearch.features.account.about

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseFragment
import com.appetiser.moviesearch.databinding.FragmentWebViewBinding

class WebViewFragment : BaseFragment<FragmentWebViewBinding>() {

    private val args by navArgs<WebViewFragmentArgs>()

    override fun getLayoutId() = R.layout.fragment_web_view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.aboutWebView.apply {
            settings.javaScriptEnabled = true
            loadUrl(args.url)
        }
    }
}
