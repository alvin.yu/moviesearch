package com.appetiser.moviesearch.features.account

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.user.User
import com.appetiser.moviesearch.features.account.AccountState
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class AccountViewModel @Inject constructor(
    private val sessionRepository: SessionRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<AccountState>()
    }

    val state: Observable<AccountState> = _state

    private val _user by lazy {
        MutableLiveData<User>()
    }

    val user: LiveData<User> = _user

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadAccount() {
        sessionRepository
            .getSession()
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    _user.value = session.user
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }
}
