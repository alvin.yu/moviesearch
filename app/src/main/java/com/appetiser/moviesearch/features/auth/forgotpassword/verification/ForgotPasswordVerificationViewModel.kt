package com.appetiser.moviesearch.features.auth.forgotpassword.verification

import android.os.Bundle
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.utils.COUNTDOWN_MAX_TIMER
import com.appetiser.moviesearch.utils.ResourceManager
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ForgotPasswordVerificationViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private lateinit var username: String

    private val _state by lazy {
        PublishSubject.create<ForgotPasswordVerificationState>()
    }

    val state: Observable<ForgotPasswordVerificationState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setUsername(username: String) {
        this.username = username

        _state.onNext(
            ForgotPasswordVerificationState.GetUsername(username)
        )
    }

    fun resendCode() {
        disposables.add(repository.forgotPassword(username)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                startTimer()
            }
            .doOnSuccess {
                hideLoading()
            }
            .doOnError {
                hideLoading()
            }
            .subscribeBy(onSuccess = {
                _state.onNext(ForgotPasswordVerificationState.ResendTokenSuccess)
            }, onError = {
                Timber.e(it)
                // for count down timer
                disposables.clear()
                _state.onNext(ForgotPasswordVerificationState.ResendTimerCompleted)

                showGenericError()
            })
        )
    }

    private fun hideLoading() {
        _state
            .onNext(
                ForgotPasswordVerificationState.HideProgressLoading
            )
    }

    private fun showLoading() {
        _state
            .onNext(
                ForgotPasswordVerificationState.ShowProgressLoading
            )
    }

    private fun showGenericError() {
        _state
            .onNext(
                ForgotPasswordVerificationState.Error(
                    Throwable(resourceManager.getString(
                        R.string.generic_error_short
                    ))
                )
            )
    }

    private fun startTimer() {
        Observable.interval(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(schedulers.ui())
            .map {
                (COUNTDOWN_MAX_TIMER - it)
            }
            .doOnNext {
                _state.onNext(ForgotPasswordVerificationState.ResendTimer(it))
            }
            .doOnSubscribe {
                _state.onNext(ForgotPasswordVerificationState.ResendTimer(60))
                _state.onNext(ForgotPasswordVerificationState.ShowResendProgressLoading)
            }
            .doFinally {
                _state.onNext(ForgotPasswordVerificationState.HideResendProgressLoading)
            }
            .takeUntil {
                it <= 0
            }
            .subscribeBy(
                onNext = {
                    Timber.d("Time $it")
                },
                onComplete = {
                    _state.onNext(ForgotPasswordVerificationState.ResendTimerCompleted)
                },
                onError = {
                    Timber.e("Error $it")
                }
            )
            .addTo(disposables)
    }

    fun sendToken(token: String) {
        disposables.add(repository.forgotPasswordCheckCode(username, token)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(ForgotPasswordVerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
            }
            .subscribeBy(onSuccess = {
                _state.onNext(ForgotPasswordVerificationState.ForgotPasswordSuccess(username, token))
            }, onError = {
                _state.onNext(ForgotPasswordVerificationState.Error(it))
            })
        )
    }
}
