package com.appetiser.moviesearch.features.account.changepassword.currentpassword

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import com.appetiser.module.common.extensions.*
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentChangeCurrentPasswordBinding
import com.appetiser.moviesearch.ext.setCustomPasswordTransformation
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeCurrentPasswordFragment : BaseViewModelFragment<FragmentChangeCurrentPasswordBinding, ChangeCurrentPasswordViewModel>() {

    override fun canBack(): Boolean = true

    override fun getLayoutId() = R.layout.fragment_change_current_password

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupViews() {
        binding.passwordMaxLength = PASSWORD_MAX_LENGTH
        binding.inputLayoutPassword.setCustomPasswordTransformation()

        binding.inputPassword.apply {
            textChanges()
                .skipInitialValue()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = { text ->
                        viewModel.onPasswordTextChanged(text.toString())
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)

            editorActionEvents()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            binding.inputLayoutPassword.error = ""
                            binding.root.hideKeyboard()
                            viewModel
                                .verifyPassword(
                                    binding.inputPassword.text.toString().trim()
                                )
                        }
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPassword.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .verifyPassword(
                        binding.inputPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangeCurrentPasswordState) {
        when (state) {
            ChangeCurrentPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangeCurrentPasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangeCurrentPasswordState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangeCurrentPasswordState.HideLoading -> {
                binding.loading setVisible false
            }
            ChangeCurrentPasswordState.PasswordBelowMinLength -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.password_below_minimum_format)
            }
            ChangeCurrentPasswordState.PasswordExceedsMaxLength -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.password_exceeds_maximum_format)
            }
            ChangeCurrentPasswordState.InvalidPassword -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.invalid_password)
            }
            is ChangeCurrentPasswordState.VerificationSuccess -> {
                navigate(
                    ChangeCurrentPasswordFragmentDirections.actionChangeCurrentPasswordFragmentToChangeNewPasswordFragment(
                        state.oldPassword,
                        state.verificationToken
                    )
                )
            }
            is ChangeCurrentPasswordState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }
}
