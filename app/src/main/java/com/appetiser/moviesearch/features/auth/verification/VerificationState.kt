package com.appetiser.moviesearch.features.auth.verification

sealed class VerificationState {
    data class FillUsername(val username: String) : VerificationState()

    object AuthVerificationMobileLoginType : VerificationState()

    object AuthVerificationEmailLoginType : VerificationState()

    object AuthVerificationMobileRegistrationType : VerificationState()

    object AuthVerificationEmailRegistrationType : VerificationState()

    object NavigateToAddEmailScreen : VerificationState()

    object NavigateToInputNameScreen : VerificationState()

    object ResendCodeToEmailSuccess : VerificationState()

    object ResendCodeToMobileSuccess : VerificationState()

    data class Error(val throwable: Throwable) : VerificationState()

    object ShowProgressLoading : VerificationState()

    object HideProgressLoading : VerificationState()

    object NavigateToUploadPhotoScreen : VerificationState()

    object NavigateToMainScreen : VerificationState()

    object SetVerificationModeEmail : VerificationState()

    object SetVerificationModeMobile : VerificationState()

    object NavigateUp : VerificationState()

    data class ResendTimer(val time: Long) : VerificationState()

    object ResendTimerCompleted : VerificationState()

    object ShowResendProgressLoading : VerificationState()

    object HideResendProgressLoading : VerificationState()
}
