package com.appetiser.moviesearch.features.home

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.setVisible
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentMovieDetailBinding
import com.appetiser.moviesearch.ext.loadImageUrl
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class MovieDetailFragment : BaseViewModelFragment<FragmentMovieDetailBinding, MovieViewModel>() {

    private val args by navArgs<MovieDetailFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_movie_detail

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupVmObservers()

        viewModel.loadMovie(args.movieId)
    }

    private fun setupToolbar() {
        binding.toolbar.toolbarView.title = ""
    }

    private fun setupVmObservers() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                },
                onError = {
                })
            .addTo(disposables)
    }

    private fun handleState(state: MovieListState) {
        when (state) {
            is MovieListState.DisplayMovie -> {
                binding.apply {
                    state.movie.apply {
                        toolbar.toolbarView.title = trackName

                        movieDetailTrackname.text = trackName
                        movieDetailGenre.text = primaryGenreName
                        movieDetailLongDescription.text = longDescription
                        movieDetailPrice.text = trackPrice.toString()
                        movieDetailImage.loadImageUrl(artworkUrl100)

                        detailPlaceholder setVisible false
                        cardMovieDetail setVisible true
                    }
                }
            }
        }
    }
}
