package com.appetiser.moviesearch.features.auth.forgotpassword.newpassword

sealed class NewPasswordState {

    data class GetUsername(val username: String) : NewPasswordState()

    data class Success(val username: String) : NewPasswordState()

    data class Error(val throwable: Throwable) : NewPasswordState()

    object PasswordBelowMinLength : NewPasswordState()

    object PasswordExceedsMaxLength : NewPasswordState()

    object ShowProgressLoading : NewPasswordState()

    object HideProgressLoading : NewPasswordState()
}
