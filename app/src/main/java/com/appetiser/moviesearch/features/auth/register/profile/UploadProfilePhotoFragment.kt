package com.appetiser.moviesearch.features.auth.register.profile

import android.Manifest
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseCameraFragment
import com.appetiser.moviesearch.databinding.FragmentUploadProfilePhotoBinding
import com.appetiser.moviesearch.ext.*
import com.appetiser.moviesearch.utils.ViewUtils
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class UploadProfilePhotoFragment : BaseCameraFragment<FragmentUploadProfilePhotoBinding, UploadPhotoViewModel>() {

    private val args: UploadProfilePhotoFragmentArgs by navArgs()

    override fun imageUrlPath(captureCameraPaths: List<String>) {
        if (captureCameraPaths.isNotEmpty()) {
            binding.ivProfilePhoto.triggerChanges()
            binding.ivProfilePhoto.loadAvatarUrl(captureCameraPaths[0])
        } else {
            ViewUtils.showGenericErrorSnackBar(
                binding.root,
                getString(R.string.invalid_file)
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_upload_profile_photo

    override fun canBack() = !args.hideBackButton

    override fun isExitOnBack(): Boolean = args.isExitOnBack

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupViews()
        observeViews()
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState
            )
            .addTo(disposables)
    }

    private fun handleState(state: UploadPhotoState) {
        when (state) {
            is UploadPhotoState.ErrorUploadPhoto -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.throwable.getThrowableError()
                )
            }
            UploadPhotoState.ShowSubscriptionScreen -> {
                navigateToSubscriptionScreen()
            }
            UploadPhotoState.ShowProgressLoading -> {
                binding.loading setVisible true
            }
            UploadPhotoState.HideProgressLoading -> {
                binding.loading setVisible false
            }
        }
    }

    private fun navigateToSubscriptionScreen() {
        navigate(
            UploadProfilePhotoFragmentDirections.actionUploadProfilePhotoFragmentToSubscriptionFragment()
        )
    }

    private fun setupToolbar() {
        binding.toolbar.apply {
            if (!args.hideBackButton) {
                enableToolbarHomeIndicator(toolbarView)
            }

            binding.toolbar.toolbarButton.apply {
                text = getString(R.string.skip)
                setTextColor(
                    requireContext()
                        .getThemeColor(
                            R.attr.colorGreyInactive
                        )
                )

                ninjaTap {
                    viewModel.onBoardingComplete()
                }.addTo(disposables)
            }
        }
    }

    private fun observeViews() {
        val imageObserver = binding.ivProfilePhoto.imageChanges()
            .map { it }

        imageObserver
            .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onError = {
                    requireActivity().toast(it.message.orEmpty())
                },
                onNext = {
                    if (it) {
                        binding.indicatorContainer.setProgressCount(3)
                        binding.btnContinue.visibility = View.VISIBLE
                        binding.btnContinue.enabledWithAlpha()
                    } else {
                        binding.btnContinue.visibility = View.INVISIBLE
                        binding.btnContinue.disabledWithAlpha()
                    }
                })
            .addTo(disposables)
    }

    private fun setupViews() {
        binding.btnCamera.ninjaTap {
            rxPermissions
                .requestEachCombined(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .doOnNext {
                    when {
                        it.granted -> {
                            showPickerDialog()
                        }
                        else -> {
                            hidePickerDialog()
                        }
                    }
                }
                .subscribe()
        }

        binding.btnContinue.ninjaTap {
            viewModel.uploadPhoto(cameraPathUrls)
        }.addTo(disposables)
    }
}
