package com.appetiser.moviesearch.features.auth.verification

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.activity.addCallback
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.MainGraphDirections
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentRegisterVerificationCodeBinding
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.features.main.MainActivity
import com.appetiser.moviesearch.utils.NavigationUtils
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.appcompat.navigationClicks
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class VerificationFragment : BaseViewModelFragment<FragmentRegisterVerificationCodeBinding, VerificationViewModel>() {

    private val args by navArgs<VerificationFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_register_verification_code

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        check(args.email.isNotEmpty() || args.phone.isNotEmpty()) {
            "Required parameter email or phone not found!"
        }

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(viewLifecycleOwner) {
                viewModel.onBackClick()
            }

        setupViews()
        setupToolbar()
        setupViewModels()
        observeInputViews()

        viewModel.setEmailOrPhoneOrVerificationType(
            args.email,
            args.phone,
            args.type
        )
    }

    private fun setupToolbar() {

        binding.toolbar.toolbarView.apply {
            setNavigationIcon(R.drawable.ic_back)
            enableToolbarHomeIndicator(this)

            navigationClicks()
                .throttleFirst(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        viewModel.onBackClick()
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }
    }

    private fun setupViews() {
        binding
            .inputCode
            .editorActionEvents()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = {
                    if (it.actionId == EditorInfo.IME_ACTION_GO) {
                        verifyCode()
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding.btnResend.ninjaTap {
            viewModel.resendToken()
        }.addTo(disposables)

        binding
            .btnSkip
            .ninjaTap {
                viewModel.skipVerification()
            }
            .addTo(disposables)
    }

    private fun verifyCode() {
        viewModel.sendCode(binding.inputCode.text.toString())
    }

    private fun observeInputViews() {
        binding.inputCode.textChangeEvents()
            .skipInitialValue()
            .observeOn(scheduler.ui())
            .map { it.text }
            .map {
                it.isNotEmpty() && it.length >= 5
            }
            .subscribeBy(onNext = {
                if (it) {
                    viewModel.sendCode(binding.inputCode.text.toString())
                }
            }, onError = {
                Timber.e(it)
            }).apply { disposables.add(this) }
    }

    private fun setupViewModels() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e("Error $it")
                }
            ).addTo(disposables)
    }

    private fun handleState(state: VerificationState) {
        when (state) {
            VerificationState.SetVerificationModeEmail -> {
                binding.txtTitle.text = getString(R.string.verify_email)
            }
            VerificationState.SetVerificationModeMobile -> {
                binding.txtTitle.text = getString(R.string.verify_number)
                binding.btnSkip setVisible false
            }
            is VerificationState.FillUsername -> {
                binding.txtUsername.text = state.username
            }
            is VerificationState.AuthVerificationEmailLoginType -> {
                binding.txtTitle.text = getString(R.string.welcome_back)
                binding.txtSubtitle.text = getString(R.string.enter_the_code_sent_to)
            }
            is VerificationState.AuthVerificationMobileLoginType -> {
                binding.txtTitle.text = getString(R.string.welcome_back)
                binding.txtSubtitle.text = getString(R.string.enter_the_code_sent_to)
            }
            is VerificationState.AuthVerificationEmailRegistrationType -> {
                binding.txtSubtitle.text = getString(R.string.enter_the_code_sent_to)
            }
            is VerificationState.AuthVerificationMobileRegistrationType -> {
                binding.txtSubtitle.text = getString(R.string.verify_number_description)
            }
            is VerificationState.ResendCodeToEmailSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.resent_code_to_email)
                )
            }

            is VerificationState.ResendCodeToMobileSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.resent_code_to_mobile_number)
                )
            }

            is VerificationState.NavigateToAddEmailScreen -> {
                navigate(
                    VerificationFragmentDirections.actionRegisterVerificationFragmentToOnboardingGraph()
                )
            }
            is VerificationState.NavigateToInputNameScreen -> {
                NavigationUtils.navigateToInputNameScreen(
                    mainActivity = requireActivity() as MainActivity,
                    hideBackButton = true,
                    isExitOnBack = true
                )
            }
            is VerificationState.Error -> {
                requireActivity()
                    .toast(state.throwable.getThrowableError())
            }
            is VerificationState.ShowProgressLoading -> {
                binding.loading setVisible true
            }
            is VerificationState.HideProgressLoading -> {
                binding.loading setVisible false
            }
            is VerificationState.NavigateToMainScreen -> {
                navigateToMainScreen()
            }
            is VerificationState.NavigateToUploadPhotoScreen -> {
                NavigationUtils.navigateToUploadProfilePhotoScreen(
                    mainActivity = requireActivity() as MainActivity,
                    hideBackButton = false,
                    isExitOnBack = true
                )
            }
            is VerificationState.NavigateUp -> {
                navigateUp()
            }
            is VerificationState.ResendTimer -> {
                with(binding.btnResend) {
                    if (isEnabled) {
                        isEnabled = false
                    }
                    text = state.time.toString()
                }
            }

            is VerificationState.ResendTimerCompleted -> {
                with(binding.btnResend) {
                    isEnabled = true
                    text = getString(R.string.resend)
                }
            }

            is VerificationState.ShowResendProgressLoading -> {
                binding.progress.visible()
            }
            is VerificationState.HideResendProgressLoading -> {
                binding.progress.gone()
            }
        }
    }

    private fun navigateToMainScreen() {
        navigate(
            MainGraphDirections
                .actionGlobalToHomeGraph()
        )
    }
}
