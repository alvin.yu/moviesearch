package com.appetiser.moviesearch.features.account.primaryemail

sealed class PrimaryEmailPasswordState {
    object InitializeChangeEmail : PrimaryEmailPasswordState()

    object InitializeChangePhoneNumber : PrimaryEmailPasswordState()

    object InitializeChangePassword : PrimaryEmailPasswordState()

    object InitializeDeleteAccount : PrimaryEmailPasswordState()

    object EnableButton : PrimaryEmailPasswordState()

    object DisableButton : PrimaryEmailPasswordState()

    object ShowLoading : PrimaryEmailPasswordState()

    object HideLoading : PrimaryEmailPasswordState()

    object InvalidPassword : PrimaryEmailPasswordState()

    data class PhonePasswordVerificationSuccess(val verificationToken: String) : PrimaryEmailPasswordState()

    data class EmailPasswordVerificationSuccess(val verificationToken: String) : PrimaryEmailPasswordState()

    data class CurrentPasswordVerificationSuccess(val verificationToken: String) : PrimaryEmailPasswordState()

    object NavigateToWalkthroughScreen : PrimaryEmailPasswordState()

    data class Error(val message: String) : PrimaryEmailPasswordState()
}
