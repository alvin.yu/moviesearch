package com.appetiser.moviesearch.features.profile.editprofile

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import androidx.activity.addCallback
import com.appetiser.module.common.extensions.*
import com.appetiser.module.domain.utils.PROFILE_PHOTO_MAX_DIMENSION_PX
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseCameraFragment
import com.appetiser.moviesearch.databinding.FragmentEditProfileBinding
import com.appetiser.moviesearch.ext.loadAvatarUrl
import com.appetiser.moviesearch.utils.DateUtils
import com.appetiser.moviesearch.utils.FileUtils
import com.appetiser.moviesearch.utils.FileUtils.PHOTO_EXTENSION
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.textChanges
import com.yalantis.ucrop.UCrop
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.io.File
import java.time.LocalDate
import java.util.*
import java.util.concurrent.TimeUnit

class EditProfileFragment : BaseCameraFragment<FragmentEditProfileBinding, EditProfileViewModel>(), DatePickerDialog.OnDateSetListener {

    override fun getLayoutId(): Int = R.layout.fragment_edit_profile

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(viewLifecycleOwner) {
                viewModel.onBackPressed(
                    binding.inputName.text.toString().trim(),
                    binding.inputDescription.text.toString().trim()
                )
            }

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.loadProfile()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: EditProfileState) {
        when (state) {
            is EditProfileState.PreFillFields -> {
                preFillFields(state)
            }
            EditProfileState.EnableSaveButton -> {
                binding
                    .toolbar
                    .toolbarButton
                    .isEnabled = true
            }
            EditProfileState.DisableSaveButton -> {
                binding
                    .toolbar
                    .toolbarButton
                    .isEnabled = false
            }
            is EditProfileState.ShowDatePicker -> {
                showDatePickerDialog(
                    state.year,
                    state.month,
                    state.day
                )
            }
            is EditProfileState.DescriptionExceedsMaxLength -> {
                getString(
                    R.string.description_exceeds_maximum_format,
                    state.maxLength
                ).run {
                    binding
                        .inputLayoutDescription
                        .error = this
                }
            }
            EditProfileState.ShowLoading -> {
                binding.loading.setVisible(true)
            }
            EditProfileState.HideLoading -> {
                binding.loading.setVisible(false)
            }
            is EditProfileState.DisplayNewImage -> {
                binding
                    .imgProfile
                    .loadAvatarUrl(
                        state.imageLocalPath
                    )
            }
            is EditProfileState.DisplayDob -> {
                displayDob(
                    state.year,
                    state.month,
                    state.day
                )
            }
            EditProfileState.NavigateUp -> {
                navigateUp()
            }
            EditProfileState.ShowUnsavedChangesDialog -> {
                showUnsavedChangesDialog()
            }
            EditProfileState.UpdateProfileSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.your_changes_has_been_saved)
                )
            }
            is EditProfileState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun preFillFields(state: EditProfileState.PreFillFields) {
        binding
            .imgProfile
            .loadAvatarUrl(
                state.imageUrl
            )

        binding
            .inputName
            .apply {
                setText(
                    state.fullName
                )
                setSelection(
                    state.fullName.length
                )
            }

        binding
            .inputDescription
            .setText(
                state.description
            )
    }

    private fun displayDob(
        year: Int,
        month: Int,
        day: Int
    ) {
        val dateOfBirth = DateUtils
            .formatDate(
                LocalDate.of(
                    year,
                    month,
                    day
                )
            )

        binding
            .inputDob
            .setText(
                dateOfBirth
            )
    }

    private fun setupViews() {
        binding
            .viewDobClickArea
            .ninjaTap {
                viewModel.onDatePickerClick()
            }
            .addTo(disposables)

        binding
            .inputName
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onNameTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnCamera
            .ninjaTap {
                showImagePicker()
            }
            .addTo(disposables)
    }

    private fun showImagePicker() {
        rxPermissions
            .requestEachCombined(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .doOnNext {
                when {
                    it.granted -> {
                        showPickerDialog()
                    }
                    else -> {
                        hidePickerDialog()
                    }
                }
            }
            .subscribe()
            .addTo(disposables)
    }

    private fun showDatePickerDialog(
        year: Int,
        month: Int,
        day: Int
    ) {
        val datePicker = DatePickerDialog(
            requireContext(),
            this,
            year,
            month - 1,
            day
        )
        datePicker.datePicker.maxDate = Date().time
        datePicker.show()
    }

    private fun showUnsavedChangesDialog() {
        ViewUtils.showConfirmDialog(
            requireContext(),
            getString(R.string.unsaved_changes_title),
            getString(R.string.unsaved_changes_body),
            getString(R.string.discard),
            getString(R.string.back),
            positiveClickListener = {
                navigateUp()
            }
        )
    }

    override fun onDateSet(
        datePicker: DatePicker,
        year: Int,
        monthOfYear: Int,
        dayOfMonth: Int
    ) {
        viewModel
            .onDobSelected(
                year,
                monthOfYear + 1,
                dayOfMonth
            )
    }

    override fun imageUrlPath(captureCameraPaths: List<String>) {
        if (captureCameraPaths.isNotEmpty()) {
            val destinationUri =
                Uri.fromFile(
                    FileUtils.createTempFile(requireContext(), PHOTO_EXTENSION)
                )

            UCrop
                .of(
                    Uri.fromFile(File(captureCameraPaths[0])),
                    destinationUri
                )
                .withAspectRatio(1f, 1f)
                .withMaxResultSize(PROFILE_PHOTO_MAX_DIMENSION_PX, PROFILE_PHOTO_MAX_DIMENSION_PX)
                .start(requireContext(), this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP -> {
                viewModel
                    .onProfilePhotoChanged(
                        UCrop.getOutput(data!!)!!.path!!
                    )
            }
            resultCode == UCrop.RESULT_ERROR -> {
                if (data != null) {
                    requireActivity()
                        .toast(
                            UCrop.getError(data)?.message ?: getString(R.string.generic_error)
                        )
                } else {
                    requireActivity()
                        .toast(
                            getString(R.string.generic_error)
                        )
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(
            binding.toolbar.toolbarView,
            getString(R.string.edit_profile)
        )

        binding.toolbar.toolbarButton.apply {
            text = getString(R.string.save)

            ninjaTap {
                clearErrors()
                binding
                    .root
                    .hideKeyboard()

                viewModel
                    .saveProfile(
                        binding.inputName.text.toString().trim(),
                        binding.inputDescription.text.toString().trim()
                    )
            }.addTo(disposables)
        }
    }

    private fun clearErrors() {
        binding
            .inputLayoutName
            .error = ""

        binding
            .inputLayoutDescription
            .error = ""
    }
}
