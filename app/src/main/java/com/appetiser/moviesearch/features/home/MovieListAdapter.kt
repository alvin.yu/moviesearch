package com.appetiser.moviesearch.features.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.module.domain.models.movie.Movie
import com.appetiser.moviesearch.databinding.ItemMovieBinding
import com.appetiser.moviesearch.databinding.ItemTextviewBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val ITEM_VIEW_TYPE_HEADER = 0
private const val ITEM_VIEW_TYPE_ITEM = 1

class MovieListAdapter(private val clickListener: MovieListener)
    : ListAdapter<DataItem, RecyclerView.ViewHolder>(MovieDiffCallback()) {

    private val adapterScope = CoroutineScope(Dispatchers.Default)
    
    private var lastUserVisit: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> TextViewHolder.from(parent, lastUserVisit)
            ITEM_VIEW_TYPE_ITEM -> MovieViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    fun addHeaderAndSubmitList(list: List<Movie>?) {
        adapterScope.launch {
            val items = when (list) {
                null -> listOf(DataItem.Header)
                else -> listOf(DataItem.Header) + list.map { DataItem.MovieItem(it) }
            }
            withContext(Dispatchers.Main) {
                submitList(items)
            }
        }
    }

    fun updateHeader(newLastUserVisit: String) {
        adapterScope.launch {
            lastUserVisit = newLastUserVisit
            withContext(Dispatchers.Main) {
                notifyItemChanged(0)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MovieViewHolder -> {
                val item = getItem(position) as DataItem.MovieItem
                holder.bind(clickListener, item.movie)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            is DataItem.MovieItem -> ITEM_VIEW_TYPE_ITEM
        }
    }

}

/**
 *  Holds the TextView of RecyclersView's header.
 */
class TextViewHolder(view: View): RecyclerView.ViewHolder(view) {
    companion object {
        fun from(parent: ViewGroup, lastUserVisit: String): TextViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemTextviewBinding.inflate(layoutInflater)
            binding.itemTextview.text = lastUserVisit
            return TextViewHolder(binding.root)
        }
    }
}

class MovieViewHolder(val binding: ItemMovieBinding)
    : RecyclerView.ViewHolder(binding.root) {

    fun bind(clickListener: MovieListener, item: Movie) {
        binding.movie = item
        binding.clickListener = clickListener
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): MovieViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemMovieBinding.inflate(inflater, parent, false)
            return MovieViewHolder(binding)
        }
    }
}

class MovieDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }
}

class MovieListener(val clickListener: (movie: Movie) -> Unit) {
    fun onClick(movie: Movie) = clickListener(movie)
}