package com.appetiser.moviesearch.features.account.primaryphone

import android.os.Bundle
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.utils.COUNTDOWN_MAX_TIMER
import com.appetiser.moviesearch.utils.PhoneNumberHelper
import com.appetiser.moviesearch.utils.ResourceManager
import com.google.i18n.phonenumbers.PhoneNumberUtil
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PrimaryPhoneVerificationViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val sessionRepository: SessionRepository,
    private val resourceManager: ResourceManager,
    private val phoneNumberHelper: PhoneNumberHelper
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<PrimaryPhoneVerificationState>()
    }

    val verificationState: Observable<PrimaryPhoneVerificationState> = _state

    private lateinit var phoneNumber: String
    private lateinit var verificationScreenFlow: PrimaryPhoneVerificationScreenFlow

    private val countryNameCode by lazy {
        resourceManager.getDeviceCountryNameCode()
    }

    /**
     * Copy of previously failed code.
     *
     * This is so we don't repetitively send wrong code when user inputs the same one.
     */
    private var failedCode: String = ""

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        sessionRepository.getSession()
            .flatMap { session ->
                authRepository.generateOtp(session.user.phoneNumber)
                    .map {
                        session.user
                    }
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    setPhoneNumber(it.phoneNumber)
                }
            )
            .addTo(disposables)
    }

    fun setScreenFlow(verificationScreenFlow: PrimaryPhoneVerificationScreenFlow) {
        this.verificationScreenFlow = verificationScreenFlow

        Observable.just(verificationScreenFlow)
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = {
                    when (verificationScreenFlow) {
                        PrimaryPhoneVerificationScreenFlow.CHANGE_PHONE -> {
                            _state.onNext(PrimaryPhoneVerificationState.InitializeChangePhoneNumber)
                        }
                        PrimaryPhoneVerificationScreenFlow.CHANGE_EMAIL -> {
                            _state.onNext(PrimaryPhoneVerificationState.InitializeChangeEmail)
                        }
                        else -> {
                            _state.onNext(PrimaryPhoneVerificationState.InitializeDeleteAccount)
                        }
                    }
                }
            )
            .addTo(disposables)
    }

    private fun setPhoneNumber(
        phoneNumber: String
    ) {
        this.phoneNumber = phoneNumber

        _state
            .onNext(
                PrimaryPhoneVerificationState.DisplayPhone(
                    phoneNumberHelper
                        .getInternationalFormattedPhoneNumber(phoneNumber, countryNameCode, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
                        .replace("-", " ")
                )
            )
    }

    fun onCodeTextChanged(code: String) {
        if (code.length < 5 || code == failedCode) return

        authRepository
            .verifyPhoneNumber(
                code
            )
            .flatMap { verificationToken ->
                if (verificationScreenFlow == PrimaryPhoneVerificationScreenFlow.CHANGE_DELETE) {
                    authRepository
                        .deleteAccount(verificationToken)
                        .andThen(Single.just(verificationToken))
                } else {
                    Single.just(verificationToken)
                }
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        PrimaryPhoneVerificationState.HideKeyboard
                    )
                showLoading()
            }
            .doOnError {
                hideLoading()
            }
            .doOnSuccess {
                hideLoading()
            }
            .subscribeBy(
                onSuccess = {
                    when (verificationScreenFlow) {
                        PrimaryPhoneVerificationScreenFlow.CHANGE_PHONE -> {
                            _state
                                .onNext(
                                    PrimaryPhoneVerificationState.PhoneVerificationSuccess(it)
                                )
                        }
                        PrimaryPhoneVerificationScreenFlow.CHANGE_EMAIL -> {
                            _state
                                .onNext(
                                    PrimaryPhoneVerificationState.EmailVerificationSuccess(it)
                                )
                        }
                        else -> {
                            _state
                                .onNext(
                                    PrimaryPhoneVerificationState.NavigateToWalkthroughScreen
                                )
                        }
                    }
                },
                onError = { error ->
                    Timber.e(error)
                    handleVerificationError(error, code)
                }
            )
            .addTo(disposables)
    }

    private fun handleVerificationError(error: Throwable, code: String) {
        if (error is HttpException && error.code() == 400) {
            failedCode = code
            _state
                .onNext(
                    PrimaryPhoneVerificationState.InvalidVerificationCode(
                        error.getThrowableError()
                    )
                )
        } else {
            showGenericError()
        }
    }

    fun resendCode() {
        authRepository
            .generateOtp(
                phoneNumber
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                startTimer()
            }
            .doOnSuccess {
                hideLoading()
            }
            .doOnError {
                hideLoading()
            }
            .subscribeBy(
                onSuccess = {
                    _state
                        .onNext(
                            PrimaryPhoneVerificationState.ResendCodeSuccess
                        )
                },
                onError = {
                    Timber.e(it)
                    showGenericError()

                    disposables.clear()
                    _state.onNext(PrimaryPhoneVerificationState.ResendTimerCompleted)
                }
            )
            .addTo(disposables)
    }

    private fun showLoading() {
        _state
            .onNext(
                PrimaryPhoneVerificationState.ShowLoading
            )
    }

    private fun hideLoading() {
        _state
            .onNext(
                PrimaryPhoneVerificationState.HideLoading
            )
    }

    private fun showGenericError() {
        _state
            .onNext(
                PrimaryPhoneVerificationState.Error(
                    resourceManager.getString(
                        R.string.generic_error_short
                    )
                )
            )
    }

    private fun startTimer() {
        Observable.interval(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(schedulers.ui())
            .map {
                (COUNTDOWN_MAX_TIMER - it)
            }
            .doOnNext {
                _state.onNext(PrimaryPhoneVerificationState.ResendTimer(it))
            }
            .doOnSubscribe {
                _state.onNext(PrimaryPhoneVerificationState.ResendTimer(60))
                _state.onNext(PrimaryPhoneVerificationState.ShowResendProgressLoading)
            }
            .doFinally {
                _state.onNext(PrimaryPhoneVerificationState.HideResendProgressLoading)
            }
            .takeUntil {
                it <= 0
            }
            .subscribeBy(
                onNext = {
                    Timber.d("Time $it")
                },
                onComplete = {
                    _state.onNext(PrimaryPhoneVerificationState.ResendTimerCompleted)
                },
                onError = {
                    Timber.e("Error $it")
                }
            )
            .addTo(disposables)
    }
}
