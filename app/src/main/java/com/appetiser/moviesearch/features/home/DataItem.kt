package com.appetiser.moviesearch.features.home

import com.appetiser.module.domain.models.movie.Movie

sealed class DataItem {
    abstract val id: Long

    data class MovieItem(val movie: Movie): DataItem() {
        override val id = movie.trackId
    }

    object Header: DataItem() {
        override val id = Long.MIN_VALUE
    }
}