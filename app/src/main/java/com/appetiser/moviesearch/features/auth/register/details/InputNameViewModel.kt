package com.appetiser.moviesearch.features.auth.register.details

import android.os.Bundle
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.models.user.User
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class InputNameViewModel @Inject constructor(
    private val sessionRepository: SessionRepository,
    private val userRepository: UserRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    private val _state by lazy {
        PublishSubject.create<InputNameState>()
    }

    val state: Observable<InputNameState> = _state

    fun loadUserDetails() {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    _state.onNext(
                        InputNameState.PreFillName(
                            session.user.fullName
                        )
                    )
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun sendUserDetails(fullName: String) {
        userRepository
            .updateUser(
                User
                    .empty()
                    .apply {
                        this.fullName = fullName
                    }
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(InputNameState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(InputNameState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(InputNameState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = { user ->
                    if (user.fullName.isNotEmpty()) {
                        _state.onNext(InputNameState.UserDetailsUpdated(user))
                    } else {
                        _state.onNext(InputNameState.Error(Throwable("Something went wrong")))
                    }
                }, onError = {
                    _state.onNext(InputNameState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }
}
