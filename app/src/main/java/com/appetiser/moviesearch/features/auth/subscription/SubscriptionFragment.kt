package com.appetiser.moviesearch.features.auth.subscription

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.extensions.spannableString
import com.appetiser.module.common.extensions.toast
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentSubscriptionBinding
import com.appetiser.moviesearch.ext.getThemeColor
import io.reactivex.rxkotlin.addTo

class SubscriptionFragment : BaseViewModelFragment<FragmentSubscriptionBinding, SubscriptionViewModel>() {

    override fun getLayoutId(): Int = R.layout.fragment_subscription

    override fun canBack() = true

    override fun isExitOnBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        setupButtons()
        setupTermsAndPrivacy()
    }

    private fun setupButtons() {
        binding
            .btnContinue
            .ninjaTap {
                navigateToHomeScreen()
            }
            .addTo(disposables)

        binding
            .btnClose
            .ninjaTap {
                navigateToHomeScreen()
            }
    }

    private fun navigateToHomeScreen() {
        navigate(
            SubscriptionFragmentDirections.actionSubscriptionFragmentToMainGraph()
        )
    }

    private fun setupTermsAndPrivacy() {
        binding.txtTermsOfService.movementMethod = LinkMovementMethod.getInstance()
        binding.txtTermsOfService.text = getString(R.string.terms_of_service)
            .spannableString(
                requireContext(),
                null,
                R.font.inter_regular,
                requireContext().getThemeColor(R.attr.colorPrimaryFull),
                getString(R.string.terms_of_service),
                clickable = {
                    requireActivity().toast("Clicked $it")
                }
            )

        binding.txtPrivacyPolicy.movementMethod = LinkMovementMethod.getInstance()
        binding.txtPrivacyPolicy.text = getString(R.string.privacy_policy)
            .spannableString(
                requireContext(),
                null,
                R.font.inter_regular,
                requireContext().getThemeColor(R.attr.colorPrimaryFull),
                getString(R.string.privacy_policy),
                clickable = {
                    requireActivity().toast("Clicked $it")
                }
            )
    }
}
