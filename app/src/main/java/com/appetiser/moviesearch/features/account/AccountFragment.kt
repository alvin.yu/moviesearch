package com.appetiser.moviesearch.features.account

import android.os.Bundle
import android.view.View
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.extensions.setVisible
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentAccountBinding
import com.appetiser.moviesearch.utils.ViewUtils
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class AccountFragment : BaseViewModelFragment<FragmentAccountBinding, AccountViewModel>() {

    override fun getLayoutId(): Int = R.layout.fragment_account

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.vm = viewModel

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.loadAccount()
    }

    private fun setupToolbar() {
        binding.toolbar.toolbarView.title = getString(R.string.account)
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: AccountState) {
        when (state) {
            AccountState.ShowLoading -> {
                binding.loading setVisible true
            }
            AccountState.HideLoading -> {
                binding.loading setVisible false
            }
            is AccountState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .btnEditProfile
            .ninjaTap {
                navigate(
                    AccountFragmentDirections.actionAccountFragmentToEditProfileFragment()
                )
            }
            .addTo(disposables)

        binding
            .btnAccountSettings
            .ninjaTap {
                navigate(
                    AccountFragmentDirections.actionAccountFragmentToAccountSettingsGraph()
                )
            }
            .addTo(disposables)

        binding
            .btnAbout
            .ninjaTap {
                navigate(
                    AccountFragmentDirections.actionAccountFragmentToAboutFragment()
                )
            }
            .addTo(disposables)
    }
}
