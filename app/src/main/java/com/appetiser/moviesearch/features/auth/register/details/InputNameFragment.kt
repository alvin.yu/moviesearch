package com.appetiser.moviesearch.features.auth.register.details

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import com.appetiser.module.common.extensions.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.extensions.toast
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentInputNameBinding
import com.appetiser.moviesearch.ext.disabledWithAlpha
import com.appetiser.moviesearch.ext.enabledWithAlpha
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.features.main.MainActivity
import com.appetiser.moviesearch.utils.NavigationUtils
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class InputNameFragment : BaseViewModelFragment<FragmentInputNameBinding, InputNameViewModel>() {

    override fun getLayoutId(): Int = R.layout.fragment_input_name

    override fun canBack() = false

    override fun isExitOnBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViews()
        observeInputViews()
        setupViewModel()

        viewModel.loadUserDetails()
    }

    private fun setUpViews() {
        binding.inputName.apply {
            editorActionEvents()
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            if (binding.btnContinue.isEnabled) {
                                sendUserDetails()
                            }
                        }
                    }
                )
                .addTo(disposables)
        }

        binding
            .btnContinue
            .ninjaTap {
                sendUserDetails()
            }
            .addTo(disposables)
    }

    private fun sendUserDetails() {
        viewModel
            .sendUserDetails(
                binding.inputName.text.toString().trim()
            )
    }

    private fun observeInputViews() {
        val firstNameObservable = binding.inputName.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        firstNameObservable
            .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    if (it) {
                        binding.btnContinue.enabledWithAlpha()
                    } else {
                        binding.btnContinue.disabledWithAlpha()
                    }
                },
                onError = {
                    Timber.e("Error $it")
                }
            )
            .addTo(disposables)
    }

    private fun setupViewModel() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }

    private fun handleState(state: InputNameState) {
        when (state) {
            is InputNameState.UserDetailsUpdated -> {
                NavigationUtils.navigateToUploadProfilePhotoScreen(
                    mainActivity = requireActivity() as MainActivity,
                    hideBackButton = false,
                    isExitOnBack = false
                )
            }
            is InputNameState.Error -> {
                requireActivity()
                    .toast(state.throwable.getThrowableError())
            }
            is InputNameState.ShowProgressLoading -> {
            }
            InputNameState.HideProgressLoading -> {
                // TODO: 3/8/21
            }
            is InputNameState.PreFillName -> {
                binding.inputName.apply {
                    setText(state.fullName)
                    setSelection(state.fullName.length)
                }
            }
        }
    }
}
