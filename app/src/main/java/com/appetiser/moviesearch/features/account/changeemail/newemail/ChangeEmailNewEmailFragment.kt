package com.appetiser.moviesearch.features.account.changeemail.newemail

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentChangeEmailNewEmailBinding
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeEmailNewEmailFragment : BaseViewModelFragment<FragmentChangeEmailNewEmailBinding, ChangeEmailNewEmailViewModel>() {

    private val args by navArgs<ChangeEmailNewEmailFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_change_email_new_email

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setVerificationToken(args.verificationToken)

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.loadAccount()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangeEmailNewEmailState) {
        when (state) {
            ChangeEmailNewEmailState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangeEmailNewEmailState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangeEmailNewEmailState.InvalidEmail -> {
                binding
                    .inputLayoutEmail
                    .error = getString(R.string.invalid_email)
            }
            ChangeEmailNewEmailState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangeEmailNewEmailState.HideLoading -> {
                binding.loading setVisible false
            }
            is ChangeEmailNewEmailState.EmailAlreadyTaken -> {
                binding
                    .inputLayoutEmail
                    .error = state.message
            }
            is ChangeEmailNewEmailState.ChangeEmailSuccess -> {
                navigate(
                    ChangeEmailNewEmailFragmentDirections.actionChangeEmailNewEmailFragmentToChangeEmailVerifyCodeFragment(
                        state.email,
                        state.verificationToken
                    )
                )
            }
            is ChangeEmailNewEmailState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }

            is ChangeEmailNewEmailState.UpdatedEmail -> {
                navigate(
                    ChangeEmailNewEmailFragmentDirections.actionChangeEmailNewEmailFragmentToAccountSettingsGraph(true)
                )
            }
        }
    }

    private fun setupViews() {
        with(binding.inputEmail) {
            textChanges()
                .skipInitialValue()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeBy(
                    onNext = { text ->
                        viewModel.onEmailTextChanged(text.toString())
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)

            editorActionEvents()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            binding.inputLayoutEmail.error = ""
                            binding.root.hideKeyboard()
                            viewModel
                                .changeEmail(
                                    binding.inputEmail.text.toString().trim()
                                )
                        }
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutEmail.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .changeEmail(
                        binding.inputEmail.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }
}
