package com.appetiser.moviesearch.features.auth.forgotpassword

sealed class ForgotPasswordState {

    data class GetUsername(val username: String) : ForgotPasswordState()

    data class Success(val username: String) : ForgotPasswordState()

    data class Error(val throwable: Throwable) : ForgotPasswordState()

    object ShowProgressLoading : ForgotPasswordState()

    object HideProgressLoading : ForgotPasswordState()
}
