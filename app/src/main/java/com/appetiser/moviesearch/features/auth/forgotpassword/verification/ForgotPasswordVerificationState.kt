package com.appetiser.moviesearch.features.auth.forgotpassword.verification

sealed class ForgotPasswordVerificationState {

    data class GetUsername(val username: String) : ForgotPasswordVerificationState()

    object ResendTokenSuccess : ForgotPasswordVerificationState()

    data class ForgotPasswordSuccess(val username: String, val token: String) : ForgotPasswordVerificationState()

    data class Error(val throwable: Throwable) : ForgotPasswordVerificationState()

    object ShowProgressLoading : ForgotPasswordVerificationState()

    object HideProgressLoading : ForgotPasswordVerificationState()

    data class ResendTimer(val time: Long) : ForgotPasswordVerificationState()

    object ResendTimerCompleted : ForgotPasswordVerificationState()

    object ShowResendProgressLoading : ForgotPasswordVerificationState()

    object HideResendProgressLoading : ForgotPasswordVerificationState()
}
