package com.appetiser.moviesearch.features.auth.forgotpassword.newpassword

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.TEXT_WATCHER_DEBOUNCE_TIME
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.extensions.toast
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentNewPasswordBinding
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.ext.setCustomPasswordTransformation
import com.jakewharton.rxbinding3.widget.editorActionEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class NewPasswordFragment : BaseViewModelFragment<FragmentNewPasswordBinding, NewPasswordViewModel>() {

    private val args by navArgs<NewPasswordFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_new_password

    override fun canBack(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupToolbar()
        setupViewModels()

        viewModel
            .setUsernameAndToken(
                args.username,
                args.token
            )
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun setupViews() {
        binding.inputLayoutPassword.setCustomPasswordTransformation()
        binding.passwordMaxLength = PASSWORD_MAX_LENGTH
        binding.inputPassword.apply {
            editorActionEvents()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            viewModel.sendNewPassword(text.toString())
                        }
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }

        binding
            .btnContinue
            .ninjaTap {
                viewModel
                    .sendNewPassword(
                        binding.inputPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupViewModels() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }

    private fun handleState(state: NewPasswordState) {
        when (state) {
            is NewPasswordState.GetUsername -> {
                binding.txtUsername.apply {
                    text = state.username
                }
            }
            is NewPasswordState.Success -> {
                requireActivity()
                    .toast("Password successfully changed!")

                navigate(
                    NewPasswordFragmentDirections.actionNewPasswordFragmentToLoginFragment(
                        state.username
                    )
                )
            }
            is NewPasswordState.Error -> {
                requireActivity()
                    .toast(state.throwable.getThrowableError())
            }
            is NewPasswordState.ShowProgressLoading -> {
            }
            is NewPasswordState.HideProgressLoading -> {
            }
            NewPasswordState.PasswordBelowMinLength -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.password_below_minimum_format, PASSWORD_MIN_LENGTH)
            }
            NewPasswordState.PasswordExceedsMaxLength -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.password_exceeds_maximum_format, PASSWORD_MAX_LENGTH)
            }
        }
    }
}
