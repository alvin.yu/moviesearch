package com.appetiser.moviesearch.features.account.primaryemail

import android.os.Bundle
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.utils.ResourceManager
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class PrimaryEmailPasswordViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val sessionRepository: SessionRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<PrimaryEmailPasswordState>()
    }

    val state: Observable<PrimaryEmailPasswordState> = _state
    private lateinit var screenFlow: PrimaryEmailPasswordScreenFlow

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    fun setScreenFlow(screenFlow: PrimaryEmailPasswordScreenFlow) {
        this.screenFlow = screenFlow

        Observable.just(screenFlow)
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = {
                    when (screenFlow) {
                        PrimaryEmailPasswordScreenFlow.CHANGE_PHONE -> {
                            _state.onNext(PrimaryEmailPasswordState.InitializeChangePhoneNumber)
                        }
                        PrimaryEmailPasswordScreenFlow.CHANGE_EMAIL -> {
                            _state.onNext(PrimaryEmailPasswordState.InitializeChangeEmail)
                        }
                        PrimaryEmailPasswordScreenFlow.CHANGE_PASSWORD -> {
                            _state.onNext(PrimaryEmailPasswordState.InitializeChangePassword)
                        }
                        else -> {
                            _state.onNext(PrimaryEmailPasswordState.InitializeDeleteAccount)
                        }
                    }
                }
            )
            .addTo(disposables)
    }

    fun onPasswordTextChanged(password: String) {
        if (validatePasswordNotEmpty(password)) {
            _state
                .onNext(
                    PrimaryEmailPasswordState.EnableButton
                )
        } else {
            _state
                .onNext(
                    PrimaryEmailPasswordState.DisableButton
                )
        }
    }

    fun verifyPassword(password: String) {
        if (!validatePasswordNotEmpty(password)) return

        authRepository
            .verifyPassword(password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        PrimaryEmailPasswordState.ShowLoading
                    )
            }
            .doOnSuccess {
                _state
                    .onNext(
                        PrimaryEmailPasswordState.HideLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        PrimaryEmailPasswordState.HideLoading
                    )
            }
            .subscribeBy(
                onSuccess = {

                    when (screenFlow) {
                        PrimaryEmailPasswordScreenFlow.CHANGE_PHONE -> {
                            _state
                                .onNext(
                                    PrimaryEmailPasswordState.PhonePasswordVerificationSuccess(it)
                                )
                        }
                        PrimaryEmailPasswordScreenFlow.CHANGE_EMAIL -> {
                            _state
                                .onNext(
                                    PrimaryEmailPasswordState.EmailPasswordVerificationSuccess(it)
                                )
                        }
                        PrimaryEmailPasswordScreenFlow.CHANGE_PASSWORD -> {
                            _state
                                .onNext(
                                    PrimaryEmailPasswordState.CurrentPasswordVerificationSuccess(it)
                                )
                        }
                        else -> {
                            deleteAccount(it)
                        }
                    }
                },
                onError = { error ->
                    Timber.e(error)
                    if (error is HttpException && error.code() == 400) {
                        _state
                            .onNext(
                                PrimaryEmailPasswordState.InvalidPassword
                            )
                    } else {
                        showGenericError()
                    }
                }
            )
            .addTo(disposables)
    }

    private fun showGenericError() {
        _state
            .onNext(
                PrimaryEmailPasswordState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validatePasswordNotEmpty(password: String): Boolean {
        return password.isNotEmpty()
    }

    private fun deleteAccount(verificationToken: String) {
        authRepository.deleteAccount(verificationToken)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        PrimaryEmailPasswordState.ShowLoading
                    )
            }
            .doOnComplete {
                _state
                    .onNext(
                        PrimaryEmailPasswordState.HideLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        PrimaryEmailPasswordState.HideLoading
                    )
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            PrimaryEmailPasswordState.NavigateToWalkthroughScreen
                        )
                },
                onError = {
                    Timber.e("Error $it")
                    showGenericError()
                }
            )
            .addTo(disposables)
    }
}
