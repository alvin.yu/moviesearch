package com.appetiser.moviesearch.features.account.primaryphone

enum class PrimaryPhoneVerificationScreenFlow(val value: Int) {
    CHANGE_PHONE(0), CHANGE_EMAIL(1), CHANGE_DELETE(2)
}
