package com.appetiser.moviesearch.features.main

import android.os.Bundle
import com.appetiser.baseplate.features.main.MainState
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.utils.RECOMMEND_UPDATE_INTERVAL_IN_MILLIS
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val sessionRepository: SessionRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<MainState>()
    }

    val state: Observable<MainState> = _state

    private val bottomNavDestinationIds by lazy {
        arrayOf(
            R.id.homeFragment,
            R.id.accountFragment
        )
    }

    private val transparentStatusBarDestinationIds by lazy {
        arrayOf(
            R.id.walkthroughFragment,
            R.id.landingFragment,
            R.id.subscriptionFragment,
            R.id.welcomeFragment
        )
    }

    private val adjustPanSoftInputModeDestinationIds by lazy {
        arrayOf(R.id.walkthroughFragment)
    }

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun versionCheck() {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    if (session.updateGate == null) return@subscribeBy

                    handleUpdateGate(session.updateGate!!, session)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun onUpdateLaterClicked() {
        sessionRepository
            .getSession()
            .flatMap { session ->
                session.recommendUpdateTimeStamp = System.currentTimeMillis()

                sessionRepository.saveSession(session)
            }
            .ignoreElement()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onComplete = {},
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleUpdateGate(updateGate: UpdateGate, session: Session) {
        if (updateGate.isRequired) {
            _state.onNext(
                MainState.ShowUpdateGateForced(
                    updateGate.storeUrl
                )
            )
        } else {
            val recommendUpdateTimeElapsed = System.currentTimeMillis() - session.recommendUpdateTimeStamp
            val shouldRecommendUpdate = recommendUpdateTimeElapsed >= RECOMMEND_UPDATE_INTERVAL_IN_MILLIS

            // If recommend update has not yet passed interval time.
            if (!shouldRecommendUpdate) return

            _state.onNext(
                MainState.ShowUpdateGateRecommended(
                    updateGate.storeUrl
                )
            )
        }
    }

    fun onDestinationChanged(destinationId: Int) {
        handleBottomNavigation(destinationId)
        handleStatusBar(destinationId)
        handleSoftInputMode(destinationId)
    }

    private fun handleSoftInputMode(destinationId: Int) {
        when (destinationId) {
            in adjustPanSoftInputModeDestinationIds -> {
                _state
                    .onNext(
                        MainState.ChangeSoftInputModeAdjustPan
                    )
            }
            else -> {
                _state
                    .onNext(
                        MainState.ChangeSoftInputModeDefault
                    )
            }
        }
    }

    private fun handleStatusBar(destinationId: Int) {
        when (destinationId) {
            in transparentStatusBarDestinationIds -> {
                _state
                    .onNext(
                        MainState.ChangeStatusBarToDarkTransparent
                    )
            }
            else -> {
                _state
                    .onNext(
                        MainState.ChangeStatusBarToDefault
                    )
            }
        }
    }

    private fun handleBottomNavigation(destinationId: Int) {
        when (destinationId) {
            in bottomNavDestinationIds -> {
                _state
                    .onNext(
                        MainState.ShowBottomNavigation
                    )
            }
            else -> {
                _state
                    .onNext(
                        MainState.HideBottomNavigation
                    )
            }
        }
    }
}
