package com.appetiser.moviesearch.features.auth.landing

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.view.isInvisible
import androidx.transition.TransitionInflater
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentLandingBinding
import com.appetiser.moviesearch.features.auth.FacebookLoginManager
import com.appetiser.moviesearch.features.auth.GoogleSignInManager
import com.appetiser.moviesearch.features.auth.verification.AuthVerificationType
import com.appetiser.moviesearch.utils.ViewUtils
import com.google.gson.Gson
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class LandingFragment : BaseViewModelFragment<FragmentLandingBinding, LandingViewModel>() {

    private lateinit var facebookLoginManager: FacebookLoginManager
    private lateinit var googleSignInManager: GoogleSignInManager
    private val screenHeight by lazy {
        requireActivity().getDeviceHeight()
    }
    private var btnFacebookTranslationValue: Float = 0f
    private var btnGoogleTranslationValue: Float = 0f

    private var observeInput = true
    private var isInputModeFreeText = true

    override fun getLayoutId(): Int = R.layout.fragment_landing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
            TransitionInflater
                .from(requireContext())
                .inflateTransition(android.R.transition.move)
        sharedElementReturnTransition =
            TransitionInflater
                .from(requireContext())
                .inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        facebookLoginManager = FacebookLoginManager()
        googleSignInManager = GoogleSignInManager(this)

        setupViews()
        setupViewModel()

        viewModel.loadLanding()
    }

    override fun onResume() {
        super.onResume()
        isInputModeFreeText = true
    }

    override fun onDestroyView() {
        facebookLoginManager.clearListeners()
        googleSignInManager.clearListeners()
        super.onDestroyView()
    }

    private fun setupViews() {
        setupSignInSocialButtons()
        setupButtonListeners()

        with(binding.inputUsername) {
            textChanges()
                .skipInitialValue()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeBy(
                    onNext = {
                        if (!observeInput) return@subscribeBy

                        viewModel.onUsernameTextChanged(
                            getUsernameText()
                        )
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)

            editorActionEvents()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            viewModel.onContinueClick(
                                getUsernameText()
                            )
                        }
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }
    }

    private fun getUsernameText(): String {
        val username = binding.inputUsername.text.toString().trim()

        return if (binding.inputLayoutUsername.prefixText != null) {
            "${binding.inputLayoutUsername.prefixText!!.trim()}$username"
        } else {
            username
        }
    }

    private fun setupSignInSocialButtons() {
        binding.btnFacebook.post {
            btnFacebookTranslationValue = screenHeight.toFloat() - binding.btnFacebook.y
            btnGoogleTranslationValue = (screenHeight.toFloat() - binding.btnGoogle.y) + binding.btnGoogle.height
            binding.btnFacebook.translationY = btnFacebookTranslationValue
            binding.btnGoogle.translationY = btnGoogleTranslationValue
        }
    }

    private fun setupButtonListeners() {
        setupFacebookButton()
        setupGoogleButton()

        binding.btnSignInWithSocial.isActivated = true

        binding
            .btnContinue
            .ninjaTap {
                viewModel.onContinueClick(
                    getUsernameText()
                )
            }
            .addTo(disposables)

        binding
            .btnSignInWithSocial
            .ninjaTap {
                if (binding.groupSocialLogins.visibility == View.VISIBLE) {
                    animateSocialLoginHide()
                } else {
                    animateSocialLoginShow()
                }
            }
            .addTo(disposables)
    }

    private fun animateSocialLoginHide() {
        AnimatorSet().apply {
            playTogether(
                btnFacebookTranslationHide,
                btnGoogleTranslationHide,
                btnFacebookAlphaHide,
                btnGoogleAlphaHide
            )
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    binding.groupSocialLogins.isInvisible = true
                    binding.btnSignInWithSocial.isActivated = true
                }
            })
            duration = ANIMATION_SLOW_MILLIS
            start()
        }
    }

    private fun animateSocialLoginShow() {
        AnimatorSet().apply {
            playTogether(
                btnFacebookTranslationShow,
                btnGoogleTranslationShow,
                btnFacebookAlphaShow,
                btnGoogleAlphaShow
            )
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    binding.btnSignInWithSocial.isActivated = false
                }

                override fun onAnimationStart(animation: Animator?, isReverse: Boolean) {
                    super.onAnimationStart(animation, isReverse)
                    binding.groupSocialLogins.isInvisible = false
                }
            })
            duration = ANIMATION_SLOW_MILLIS
            start()
        }
    }

    private fun setupViewModel() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: LandingState) {
        when (state) {
            is LandingState.SocialLoginSuccess -> {
                navigateToHomeScreen()
            }
            is LandingState.SocialLoginSuccessButNotOnboarded -> {
                navigateToOnboardingFlow()
            }
            is LandingState.ShowLoading -> {
                showLoadingState(state.loginType)
            }
            is LandingState.HideLoading -> {
                hideLoadingState()
            }
            is LandingState.Error -> {
                requireActivity()
                    .toast(getString(R.string.generic_error))
            }

            is LandingState.WrongPrimaryAccount -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }

            LandingState.EnableContinueButton -> {
                binding.btnContinue.isEnabled = true
            }
            LandingState.DisableContinueButton -> {
                binding.btnContinue.isEnabled = false
            }
            is LandingState.EmailUsernameExists -> {
                navigate(
                    LandingFragmentDirections.actionLandingFragmentToLoginGraph(
                        email = state.email
                    )
                )
            }
            is LandingState.EmailUsernameDoesNotExists -> {
                navigate(
                    LandingFragmentDirections.actionLandingFragmentToSignupGraph(
                        email = state.email
                    )
                )
            }
            is LandingState.MobileUsernameExists -> {
                navigate(
                    LandingFragmentDirections.actionLandingFragmentToRegisterVerificationCodeGraph(
                        phone = state.mobile,
                        type = AuthVerificationType.LOGIN
                    )
                )
            }
            is LandingState.MobileUsernameDoesNotExists -> {
                viewModel.generateOTP(phone = state.mobile)
            }

            is LandingState.MobileRegistrationGenerateOTPSuccess -> {
                navigate(
                    LandingFragmentDirections.actionLandingFragmentToRegisterVerificationCodeGraph(
                        phone = state.mobile,
                        type = AuthVerificationType.REGISTRATION
                    )
                )
            }

            is LandingState.SetInputModeMobile -> {
                if (isInputModeFreeText) {
                    setInputModeMobile(state.countryCode, state.number)
                }
            }
            is LandingState.SetInputModeFreeText -> {
                if (!isInputModeFreeText) {
                    setInputModeFreeText(state.input)
                }
            }
        }
    }

    private fun setInputModeFreeText(input: String) {
        observeInput = false

        binding.inputLayoutUsername.prefixText = null
        binding.inputUsername.apply {
            inputType = EditorInfo.TYPE_CLASS_TEXT

            setText(input)
            setSelection(input.length)

            postDelayed({
                isInputModeFreeText = true
                observeInput = true
            }, TEXT_WATCHER_DEBOUNCE_TIME * 2)
        }
    }

    private fun setInputModeMobile(countryCode: String, number: String) {
        observeInput = false

        binding
            .inputLayoutUsername
            .prefixText = "+$countryCode "

        binding.inputUsername.apply {
            binding.inputUsername.inputType = EditorInfo.TYPE_CLASS_PHONE

            setText(number)
            setSelection(number.length)

            postDelayed({
                isInputModeFreeText = false
                observeInput = true
            }, TEXT_WATCHER_DEBOUNCE_TIME * 2)
        }
    }

    private fun setupFacebookButton() {
        facebookLoginManager
            .setLoginSuccessListener { result ->
                Timber.d(Gson().toJson(result))
                viewModel.onFacebookLogin(result.accessToken.token)
            }

        facebookLoginManager
            .setLoginErrorListener {
                requireActivity()
                    .toast(getString(R.string.generic_error))
            }

        binding
            .btnFacebook
            .ninjaTap {
                Timber.d("clicked!")
                facebookLoginManager.login(this)
            }
            .apply { disposables.add(this) }
    }

    private fun setupGoogleButton() {
        googleSignInManager
            .setSignInSuccessListener { account ->
                Timber.d(Gson().toJson(account))
                viewModel.onGoogleSignIn(account.idToken!!)
            }

        googleSignInManager
            .setSignInErrorListener {
                requireActivity()
                    .toast(getString(R.string.generic_error))
            }

        binding
            .btnGoogle
            .ninjaTap {
                googleSignInManager.signIn()
            }
            .apply { disposables.add(this) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GoogleSignInManager.REQUEST_CODE_GOOGLE_SIGN_IN) {
            googleSignInManager.sendSignInResult(data)
        } else {
            facebookLoginManager.sendLoginResult(requestCode, resultCode, data)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun showLoadingState(loginType: String) {
        when (loginType) {
            LandingViewModel.FACEBOOK -> {
                binding
                    .progressFacebook
                    .setVisible(true)

                binding
                    .imgFacebook
                    .setHidden(true)
            }
            LandingViewModel.GOOGLE -> {
                binding
                    .progressGoggle
                    .setVisible(true)

                binding
                    .imgGoogle
                    .setHidden(true)
            }
            LandingViewModel.TRADITIONAL -> {
                binding.loading setVisible true
            }
        }

        binding.root.hideKeyboard()

        binding.btnContinue.apply {
            alpha = 0.5f
            isEnabled = false
        }

        binding.btnFacebook.apply {
            alpha = 0.5f
            isEnabled = false
        }

        binding.btnGoogle.apply {
            alpha = 0.5f
            isEnabled = false
        }
    }

    private fun hideLoadingState() {
        binding.progressFacebook.setVisible(false)
        binding.progressGoggle.setVisible(false)
        binding.imgFacebook.setHidden(false)
        binding.imgGoogle.setHidden(false)
        binding.loading setVisible false

        binding.btnContinue.apply {
            alpha = 1f
            isEnabled = true
        }

        binding.btnFacebook.apply {
            alpha = 1f
            isEnabled = true
        }

        binding.btnGoogle.apply {
            alpha = 1f
            isEnabled = true
        }
    }

    private fun navigateToHomeScreen() {
        navigate(
            LandingFragmentDirections.actionLandingFragmentToHomeGraph()
        )
    }

    private fun navigateToOnboardingFlow() {
        navigate(
            LandingFragmentDirections.actionLandingFragmentToOnboardingGraph()
        )
    }

    private val btnFacebookTranslationShow by lazy {
        ObjectAnimator
            .ofFloat(
                binding.btnFacebook,
                "translationY",
                btnFacebookTranslationValue,
                0f
            )
    }
    private val btnGoogleTranslationShow by lazy {
        ObjectAnimator
            .ofFloat(
                binding.btnGoogle,
                "translationY",
                btnGoogleTranslationValue,
                0f
            )
    }
    private val btnFacebookTranslationHide by lazy {
        ObjectAnimator
            .ofFloat(
                binding.btnFacebook,
                "translationY",
                0f,
                btnFacebookTranslationValue
            )
    }
    private val btnGoogleTranslationHide by lazy {
        ObjectAnimator
            .ofFloat(
                binding.btnGoogle,
                "translationY",
                0f,
                btnGoogleTranslationValue
            )
    }
    private val btnFacebookAlphaHide by lazy {
        ObjectAnimator
            .ofFloat(
                binding.btnFacebook,
                "alpha",
                1f,
                0f
            )
    }
    private val btnGoogleAlphaHide by lazy {
        ObjectAnimator
            .ofFloat(
                binding.btnGoogle,
                "alpha",
                1f,
                0f
            )
    }
    private val btnFacebookAlphaShow by lazy {
        ObjectAnimator
            .ofFloat(
                binding.btnFacebook,
                "alpha",
                0f,
                1f
            )
    }
    private val btnGoogleAlphaShow by lazy {
        ObjectAnimator
            .ofFloat(
                binding.btnGoogle,
                "alpha",
                0f,
                1f
            )
    }
}
