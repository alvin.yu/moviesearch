package com.appetiser.moviesearch.features.auth.register.email

import android.os.Bundle
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.utils.ResourceManager
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class AddEmailViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val sessionRepository: SessionRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<AddEmailState>()
    }

    val state: Observable<AddEmailState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun onEmailTextChanged(email: String) {
        if (validateEmailNotEmpty(email)) {
            _state
                .onNext(
                    AddEmailState.EnableButton
                )
        } else {
            _state
                .onNext(
                    AddEmailState.DisableButton
                )
        }
    }

    fun updateEmail(email: String) {
        if (!validateEmail(email)) return

        repository
            .setOnBoardingEmailAddress(
                email
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        AddEmailState.ShowLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        AddEmailState.HideLoading
                    )
            }
            .subscribeBy(
                onSuccess = {
                    _state
                        .onNext(
                            AddEmailState.HideLoading
                        )

                    when {
                        !it.hasFullName() -> {
                            _state
                                .onNext(
                                    AddEmailState.NavigateToInputNameScreen
                                )
                        }
                        !it.hasProfilePhoto() -> {
                            _state.onNext(
                                AddEmailState.NavigateToUploadPhotoScreen
                            )
                        }
                        else -> {
                            _state
                                .onNext(
                                    AddEmailState.NavigateToSubscriptionScreen
                                )
                        }
                    }
                },
                onError = { error ->
                    Timber.e(error)
                    handleError(error)
                }
            )
            .addTo(disposables)
    }

    private fun handleError(error: Throwable) {
        if (error is HttpException && error.code() == 422) {
            _state
                .onNext(
                    AddEmailState.EmailAlreadyTaken(
                        error.getThrowableError()
                    )
                )
        } else {
            showGenericError()
        }
    }

    private fun validateEmail(email: String): Boolean {
        if (!validateEmailNotEmpty(email)) {
            _state
                .onNext(
                    AddEmailState.DisableButton
                )
            return false
        }

        if (!resourceManager.validateEmail(email)) {
            _state
                .onNext(
                    AddEmailState.InvalidEmail
                )
            return false
        }

        return true
    }

    private fun showGenericError() {
        _state
            .onNext(
                AddEmailState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validateEmailNotEmpty(email: String): Boolean {
        return email.isNotEmpty()
    }
}
