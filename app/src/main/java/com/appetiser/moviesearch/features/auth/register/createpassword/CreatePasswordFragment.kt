package com.appetiser.moviesearch.features.auth.register.createpassword

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.TEXT_WATCHER_DEBOUNCE_TIME
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentCreatePasswordBinding
import com.appetiser.moviesearch.ext.setCustomPasswordTransformation
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class CreatePasswordFragment : BaseViewModelFragment<FragmentCreatePasswordBinding, CreatePasswordViewModel>() {

    private val args by navArgs<CreatePasswordFragmentArgs>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_create_password
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        check(args.email.isNotEmpty() || args.phone.isNotEmpty()) {
            "Required parameter email or phone not found!"
        }

        setupToolbar()
        setupViews()
        setVmObservers()

        viewModel.setUsername(args.email, args.phone)
    }

    private fun setupViews() {
        binding.inputLayoutPassword.setCustomPasswordTransformation()
        binding.inputPassword.apply {
            textChanges()
                .skipInitialValue()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onNext = { text ->
                        viewModel.onPasswordTextChange(text.toString())
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)

            editorActionEvents()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            savePassword()
                        }
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }

        binding
            .btnContinue
            .ninjaTap {
                savePassword()
            }
            .addTo(disposables)
    }

    private fun setVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: CreatePasswordState) {
        when (state) {
            is CreatePasswordState.FillUsername -> {
                binding
                    .txtUsername
                    .text = state.username
            }
            CreatePasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            CreatePasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            CreatePasswordState.PasswordBelowMinLength -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.password_below_minimum_format, PASSWORD_MIN_LENGTH)
            }
            CreatePasswordState.PasswordExceedsMaxLength -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.password_exceeds_maximum_format, PASSWORD_MAX_LENGTH)
            }
            CreatePasswordState.ShowLoading -> {
                disableFields()
            }
            CreatePasswordState.HideLoading -> {
                enableFields()
            }
            is CreatePasswordState.StartVerificationCodeScreen -> {
                navigateToVerificationCodeScreen(
                    state.email,
                    state.phone
                )
            }
            is CreatePasswordState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun enableFields() {
        binding
            .inputPassword
            .isEnabled = true

        binding
            .btnContinue
            .isEnabled = true
    }

    private fun disableFields() {
        binding
            .inputPassword
            .isEnabled = false

        binding
            .btnContinue
            .isEnabled = false
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun savePassword() {
        viewModel
            .savePassword(
                binding.inputPassword.text.toString().trim()
            )
    }

    private fun navigateToVerificationCodeScreen(email: String, phone: String) {
        navigate(
            CreatePasswordFragmentDirections.actionCreatePasswordFragmentToRegisterVerificationFragment(
                email = email,
                phone = phone,
                type = args.type
            )
        )
    }
}
