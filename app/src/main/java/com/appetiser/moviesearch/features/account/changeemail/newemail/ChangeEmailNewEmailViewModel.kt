package com.appetiser.moviesearch.features.account.changeemail.newemail

import android.os.Bundle
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.user.PrimaryUserNameType
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.utils.ResourceManager
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class ChangeEmailNewEmailViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val sessionRepository: SessionRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<ChangeEmailNewEmailState>()
    }

    val state: Observable<ChangeEmailNewEmailState> = _state

    private lateinit var verificationToken: String

    private var primaryUserNameType: PrimaryUserNameType = PrimaryUserNameType.EMAIL

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadAccount() {
        sessionRepository
            .getSession()
            .map {
                it.user.primaryUserNameType
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    primaryUserNameType = it
                }
            )
            .addTo(disposables)
    }

    fun setVerificationToken(verificationToken: String) {
        this.verificationToken = verificationToken
    }

    fun onEmailTextChanged(email: String) {
        if (validateEmailNotEmpty(email)) {
            _state
                .onNext(
                    ChangeEmailNewEmailState.EnableButton
                )
        } else {
            _state
                .onNext(
                    ChangeEmailNewEmailState.DisableButton
                )
        }
    }

    fun changeEmail(email: String) {
        if (!validateEmail(email)) return

        authRepository
            .requestChangeEmail(
                verificationToken,
                email
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        ChangeEmailNewEmailState.ShowLoading
                    )
            }
            .doOnComplete {
                _state
                    .onNext(
                        ChangeEmailNewEmailState.HideLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        ChangeEmailNewEmailState.HideLoading
                    )
            }
            .subscribeBy(
                onComplete = {
                    if (primaryUserNameType == PrimaryUserNameType.EMAIL) {
                        _state
                            .onNext(
                                ChangeEmailNewEmailState.ChangeEmailSuccess(
                                    email,
                                    verificationToken
                                )
                            )
                    } else {
                        _state
                            .onNext(
                                ChangeEmailNewEmailState.UpdatedEmail
                            )
                    }
                },
                onError = { error ->
                    Timber.e(error)
                    handleError(error)
                }
            )
            .addTo(disposables)
    }

    private fun handleError(error: Throwable) {
        if (error is HttpException && error.code() == 422) {
            _state
                .onNext(
                    ChangeEmailNewEmailState.EmailAlreadyTaken(
                        error.getThrowableError()
                    )
                )
        } else {
            showGenericError()
        }
    }

    private fun validateEmail(email: String): Boolean {
        if (!validateEmailNotEmpty(email)) {
            _state
                .onNext(
                    ChangeEmailNewEmailState.DisableButton
                )
            return false
        }

        if (!resourceManager.validateEmail(email)) {
            _state
                .onNext(
                    ChangeEmailNewEmailState.InvalidEmail
                )
            return false
        }

        return true
    }

    private fun showGenericError() {
        _state
            .onNext(
                ChangeEmailNewEmailState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validateEmailNotEmpty(email: String): Boolean {
        return email.isNotEmpty()
    }
}
