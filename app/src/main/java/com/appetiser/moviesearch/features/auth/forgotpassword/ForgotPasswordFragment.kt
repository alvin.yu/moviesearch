package com.appetiser.moviesearch.features.auth.forgotpassword

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.extensions.setVisible
import com.appetiser.module.common.extensions.toast
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentForgotPasswordBinding
import com.appetiser.moviesearch.ext.enableWithAlphaWhen
import com.appetiser.moviesearch.ext.getThrowableError
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class ForgotPasswordFragment : BaseViewModelFragment<FragmentForgotPasswordBinding, ForgotPasswordViewModel>() {

    private val args by navArgs<ForgotPasswordFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_forgot_password

    override fun canBack(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupToolbar()
        setupViewModels()

        viewModel.setUsername(args.username)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun setupViews() {
        binding.inputUsername.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        viewModel.forgotPassword(binding.inputUsername.text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .enableWithAlphaWhen(binding.inputUsername) {
                it.isNotEmpty()
            }

        disposables.add(binding.btnContinue.ninjaTap {
            viewModel.forgotPassword(binding.inputUsername.text.toString())
        })
    }

    private fun setupViewModels() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: ForgotPasswordState) {
        when (state) {
            is ForgotPasswordState.GetUsername -> {
                binding.inputUsername.apply {
                    setText(state.username)
                    setSelection(state.username.length)
                }
            }
            is ForgotPasswordState.Success -> {
                navigate(
                    ForgotPasswordFragmentDirections.actionForgotPasswordFragmentToForgotPasswordVerificationFragment(
                        state.username
                    )
                )
            }
            is ForgotPasswordState.Error -> {
                requireActivity()
                    .toast(state.throwable.getThrowableError())
            }
            is ForgotPasswordState.ShowProgressLoading -> {
                binding.loading setVisible true
            }

            is ForgotPasswordState.HideProgressLoading -> {
                binding.loading setVisible false
            }
        }
    }
}
