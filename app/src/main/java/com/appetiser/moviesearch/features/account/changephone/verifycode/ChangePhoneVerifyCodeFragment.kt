package com.appetiser.moviesearch.features.account.changephone.verifycode

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentPrimaryPhoneVerificationBinding
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangePhoneVerifyCodeFragment : BaseViewModelFragment<FragmentPrimaryPhoneVerificationBinding, ChangePhoneVerifyCodeViewModel>() {

    private val args by navArgs<ChangePhoneVerifyCodeFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_primary_phone_verification

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.setPhoneAndVerificationToken(
            args.phone,
            args.verificationToken
        )
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangePhoneVerifyCodeState) {
        when (state) {
            is ChangePhoneVerifyCodeState.DisplayPhone -> {
                binding
                    .txtPhone
                    .text = state.phoneNumber
            }
            ChangePhoneVerifyCodeState.ShowLoading -> {
                binding.root.hideKeyboard()
            }
            ChangePhoneVerifyCodeState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangePhoneVerifyCodeState.HideLoading -> {
                binding.loading setVisible false
            }
            is ChangePhoneVerifyCodeState.ResendCodeSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.resent_code_to_mobile_number)
                )
            }
            is ChangePhoneVerifyCodeState.VerificationSuccess -> {
                navigate(
                    ChangePhoneVerifyCodeFragmentDirections.actionChangePhoneVerifyCodeFragmentToAccountSettingsGraph()
                )
            }
            is ChangePhoneVerifyCodeState.InvalidVerificationCode -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
            is ChangePhoneVerifyCodeState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }

            is ChangePhoneVerifyCodeState.ResendTimer -> {
                with(binding.btnResend) {
                    if (isEnabled) {
                        isEnabled = false
                    }
                    text = state.time.toString()
                }
            }

            is ChangePhoneVerifyCodeState.ResendTimerCompleted -> {
                with(binding.btnResend) {
                    isEnabled = true
                    text = getString(R.string.resend)
                }
            }

            is ChangePhoneVerifyCodeState.ShowResendProgressLoading -> {
                binding.progress.visible()
            }
            is ChangePhoneVerifyCodeState.HideResendProgressLoading -> {
                binding.progress.gone()
            }
        }
    }

    private fun setupViews() {
        binding
            .inputCode
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel
                        .onCodeTextChanged(
                            text.toString()
                        )
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnResend
            .ninjaTap {
                viewModel
                    .resendCode()
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }
}
