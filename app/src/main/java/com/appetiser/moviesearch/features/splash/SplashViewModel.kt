package com.appetiser.moviesearch.features.splash

import android.os.Bundle
import com.appetiser.module.data.features.miscellaneous.MiscellaneousRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.moviesearch.BuildConfig
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.utils.AuthState
import com.appetiser.moviesearch.utils.AuthenticationUtils
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val sessionRepository: SessionRepository,
    private val miscellaneousRepository: MiscellaneousRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<SplashState>()
    }

    val state: Observable<SplashState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        miscellaneousRepository
            .versionCheck(BuildConfig.VERSION_NAME)
            .onErrorResumeNext(Single.just(UpdateGate.empty()))
            .flatMap { sessionRepository.getSession() }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    val user = session.user

                    if (user.isEmpty()) {
                        _state.onNext(
                            SplashState.UserIsNotLoggedIn
                        )
                        return@subscribeBy
                    }

                    when (AuthenticationUtils.getUserAuthState(user, session)) {
                        AuthState.NotVerified -> {
                            _state.onNext(
                                SplashState.UserIsNotLoggedIn
                            )
                        }
                        AuthState.NoFullName -> {
                            _state.onNext(
                                SplashState.NoFullName
                            )
                        }
                        AuthState.NoProfilePhoto -> {
                            _state.onNext(
                                SplashState.NoProfilePhoto
                            )
                        }
                        AuthState.Completed -> {
                            _state.onNext(
                                SplashState.UserIsLoggedIn
                            )
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }
}
