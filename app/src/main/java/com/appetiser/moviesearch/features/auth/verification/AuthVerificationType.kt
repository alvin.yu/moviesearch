package com.appetiser.moviesearch.features.auth.verification

enum class AuthVerificationType(val value: Int) {
    REGISTRATION(0), LOGIN(1);
}
