package com.appetiser.moviesearch.features.account.primaryphone

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.navigation.NavOptions
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.MainGraphDirections
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentPrimaryPhoneVerificationBinding
import com.appetiser.moviesearch.features.auth.FacebookLoginManager
import com.appetiser.moviesearch.features.auth.GoogleSignInManager
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class PrimaryPhoneVerificationFragment : BaseViewModelFragment<FragmentPrimaryPhoneVerificationBinding, PrimaryPhoneVerificationViewModel>() {

    private val args by navArgs<PrimaryPhoneVerificationFragmentArgs>()

    override fun canBack(): Boolean = true

    override fun getLayoutId(): Int = R.layout.fragment_primary_phone_verification

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.setScreenFlow(args.type)
    }

    private fun setupVmObservers() {
        viewModel
            .verificationState
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(verificationState: PrimaryPhoneVerificationState) {
        when (verificationState) {
            PrimaryPhoneVerificationState.InitializeChangeEmail -> {
                binding.txtTitle.text = getString(R.string.verify_number)
                binding.txtPhoneLabel.text = getString(R.string.enter_the_code_sent_to)
            }

            PrimaryPhoneVerificationState.InitializeChangePhoneNumber -> {
                binding.txtTitle.text = getString(R.string.verify_number)
                binding.txtPhoneLabel.text = getString(R.string.enter_the_code_sent_to)
            }

            PrimaryPhoneVerificationState.InitializeDeleteAccount -> {
                binding.txtTitle.text = getString(R.string.verify_number)
                binding.txtPhoneLabel.text = getString(R.string.enter_the_code_sent_to)
            }

            is PrimaryPhoneVerificationState.DisplayPhone -> {
                val phoneNumber = verificationState.phoneNumber

                binding
                    .txtPhone
                    .text = if (phoneNumber.contains("+")) phoneNumber else getString(R.string.phone_number_format, phoneNumber)
            }
            PrimaryPhoneVerificationState.ShowLoading -> {
                binding.root.hideKeyboard()
                binding.loading setVisible true
            }
            PrimaryPhoneVerificationState.HideLoading -> {
                binding.loading setVisible false
            }
            is PrimaryPhoneVerificationState.ResendCodeSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.resent_code_to_mobile_number)
                )
            }

            is PrimaryPhoneVerificationState.PhoneVerificationSuccess -> {
                navigate(
                    PrimaryPhoneVerificationFragmentDirections.actionAccountPrimaryPhoneFragmentToAccountChangePhoneNewPhoneFragment(
                        verificationState.verificationToken
                    )
                )
            }

            is PrimaryPhoneVerificationState.EmailVerificationSuccess -> {
                navigate(
                    PrimaryPhoneVerificationFragmentDirections.actionAccountPrimaryPhoneFragmentToAccountChangeEmailNewEmailFragment(
                        verificationState.verificationToken
                    )
                )
            }

            is PrimaryPhoneVerificationState.InvalidVerificationCode -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    verificationState.message
                )
            }
            is PrimaryPhoneVerificationState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    verificationState.message
                )
            }

            is PrimaryPhoneVerificationState.ResendTimer -> {
                with(binding.btnResend) {
                    if (isEnabled) {
                        setTextColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.light_slate_gray
                            )
                        )
                        isEnabled = false
                    }
                    text = verificationState.time.toString()
                }
            }

            is PrimaryPhoneVerificationState.ResendTimerCompleted -> {
                with(binding.btnResend) {
                    setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.colorPrimary
                        )
                    )
                    isEnabled = true
                    text = getString(R.string.resend)
                }
            }

            is PrimaryPhoneVerificationState.ShowResendProgressLoading -> {
                binding.progress.visible()
            }
            is PrimaryPhoneVerificationState.HideResendProgressLoading -> {
                binding.progress.gone()
            }

            is PrimaryPhoneVerificationState.NavigateToWalkthroughScreen -> {
                logoutSocialLogins()

                val navOptions = NavOptions
                    .Builder()
                    .setEnterAnim(R.anim.anim_slide_in_bottom)
                    .setExitAnim(R.anim.anim_slide_out_top)
                    .build()

                navigate(
                    MainGraphDirections
                        .actionGlobalToWalkthroughGraph(),
                    navOptions
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputCode
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel
                        .onCodeTextChanged(
                            text.toString()
                        )
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnResend
            .ninjaTap {
                viewModel
                    .resendCode()
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun logoutSocialLogins() {
        // Sign out google if it's signed in.
        if (GoogleSignInManager.isSignedIn(requireContext())) {
            GoogleSignInManager(this)
                .run {
                    signOut()
                }
        }

        // Logout facebook if it's logged in.
        if (FacebookLoginManager.isLoggedIn()) {
            FacebookLoginManager.logout()
        }
    }
}
