package com.appetiser.moviesearch.features.auth.verification

import android.os.Bundle
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.moviesearch.utils.*
import com.google.i18n.phonenumbers.PhoneNumberUtil
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class VerificationViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val sessionRepository: SessionRepository,
    private val resourceManager: ResourceManager,
    private val phoneNumberHelper: PhoneNumberHelper
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<VerificationState>()
    }

    val state: Observable<VerificationState> = _state

    private lateinit var email: String
    private lateinit var phone: String
    private lateinit var verificationType: AuthVerificationType

    private val countryNameCode by lazy {
        resourceManager.getDeviceCountryNameCode()
    }

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setEmailOrPhoneOrVerificationType(email: String, phone: String, verificationType: AuthVerificationType) {
        this.email = email
        this.phone = phone
        this.verificationType = verificationType

        if (email.isNotEmpty()) {
            _state.onNext(
                VerificationState.SetVerificationModeEmail
            )
        } else {
            _state.onNext(
                VerificationState.SetVerificationModeMobile
            )
        }

        val username = if (email.isNotEmpty()) {
            email
        } else {
            phoneNumberHelper
                .getInternationalFormattedPhoneNumber(phone, countryNameCode, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
                .replace("-", " ")
        }

        _state.onNext(
            VerificationState.FillUsername(
                username
            )
        )

        when (verificationType) {
            AuthVerificationType.LOGIN -> {
                if (email.isNotEmpty()) {
                    _state.onNext(
                        VerificationState.AuthVerificationEmailLoginType
                    )
                } else {
                    _state.onNext(
                        VerificationState.AuthVerificationMobileLoginType
                    )
                }
            }
            AuthVerificationType.REGISTRATION -> {
                if (email.isNotEmpty()) {
                    _state.onNext(
                        VerificationState.AuthVerificationEmailRegistrationType
                    )
                } else {
                    _state.onNext(
                        VerificationState.AuthVerificationMobileRegistrationType
                    )
                }
            }
        }
    }

    fun resendToken() {
        if (email.isNotEmpty()) {
            resendEmailVerification()
        } else {
            resendPhoneVerification()
        }
    }

    private fun resendEmailVerification() {
        repository
            .resendEmailVerificationCode()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                startTimer()
            }
            .doOnSuccess {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(VerificationState.ResendCodeToEmailSuccess)
                },
                onError = {
                    // for count down timer
                    disposables.clear()
                    _state.onNext(VerificationState.ResendTimerCompleted)

                    _state.onNext(VerificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    private fun resendPhoneVerification() {
        repository
            .generateOtp(phone)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                startTimer()
            }
            .doOnSuccess {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(VerificationState.ResendCodeToMobileSuccess)
                },
                onError = {
                    // for count down timer
                    disposables.clear()
                    _state.onNext(VerificationState.ResendTimerCompleted)

                    _state.onNext(VerificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun sendCode(code: String) {
        if (email.isNotEmpty()) {
            verifyAccountEmail(code)
        } else {
            if (verificationType == AuthVerificationType.LOGIN) {
                loginWithMobile(phone, code)
            } else {
                verifyAccountPhone(phone, code)
            }
        }
    }

    private fun verifyAccountEmail(code: String) {
        repository
            .verifyAccountEmail(code)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(VerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    handleNextScreen()
                },
                onError = {
                    _state.onNext(VerificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    private fun verifyAccountPhone(phone: String, code: String) {
        repository
            .registerWithMobile(phone = phone, otp = code)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(VerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    if (it.user.email.isEmpty()) {
                        _state.onNext(VerificationState.NavigateToAddEmailScreen)
                    } else {
                        handleNextScreen()
                    }
                },
                onError = {
                    _state.onNext(VerificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    private fun loginWithMobile(phone: String, otp: String) {
        repository
            .loginWithMobile(username = phone, otp = otp)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(VerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    if (it.user.email.isEmpty()) {
                        _state.onNext(VerificationState.NavigateToAddEmailScreen)
                    } else {
                        handleNextScreen()
                    }
                },
                onError = {
                    _state.onNext(VerificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    private fun handleNextScreen() {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    when (AuthenticationUtils.getUserAuthState(session.user, session)) {
                        AuthState.NotVerified -> {
                            // do nothing
                        }
                        AuthState.NoFullName -> {
                            _state.onNext(
                                VerificationState.NavigateToInputNameScreen
                            )
                        }
                        AuthState.NoProfilePhoto -> {
                            _state.onNext(
                                VerificationState.NavigateToUploadPhotoScreen
                            )
                        }
                        AuthState.Completed -> {
                            _state.onNext(
                                VerificationState.NavigateToMainScreen
                            )
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                    _state.onNext(VerificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun skipVerification() {
        sessionRepository
            .getSession()
            .flatMap { session ->
                session.hasSkippedEmailVerification = true

                sessionRepository
                    .saveSession(
                        session
                    )
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    when {
                        session.user.email.isEmpty() -> {
                            _state.onNext(
                                VerificationState.NavigateToAddEmailScreen
                            )
                        }
                        !session.user.hasFullName() -> {
                            _state.onNext(
                                VerificationState.NavigateToInputNameScreen
                            )
                        }
                        !session.user.hasProfilePhoto() -> {
                            _state.onNext(
                                VerificationState.NavigateToUploadPhotoScreen
                            )
                        }
                        else -> {
                            _state.onNext(
                                VerificationState.NavigateToMainScreen
                            )
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                    _state.onNext(VerificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun onBackClick() {
        repository
            .logout()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(VerificationState.ShowProgressLoading)
            }
            .doOnComplete {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(VerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onComplete = {
                    _state.onNext(
                        VerificationState.NavigateUp
                    )
                },
                onError = {
                    Timber.e(it)
                    _state.onNext(VerificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    private fun startTimer() {
        Observable.interval(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(schedulers.ui())
            .map {
                (COUNTDOWN_MAX_TIMER - it)
            }
            .doOnNext {
                _state.onNext(VerificationState.ResendTimer(it))
            }
            .doOnSubscribe {
                _state.onNext(VerificationState.ResendTimer(60))
                _state.onNext(VerificationState.ShowResendProgressLoading)
            }
            .doFinally {
                _state.onNext(VerificationState.HideResendProgressLoading)
            }
            .takeUntil {
                it <= 0
            }
            .subscribeBy(
                onNext = {
                    Timber.d("Time $it")
                },
                onComplete = {
                    _state.onNext(VerificationState.ResendTimerCompleted)
                },
                onError = {
                    Timber.e("Error $it")
                }
            )
            .addTo(disposables)
    }
}
