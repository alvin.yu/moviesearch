package com.appetiser.moviesearch.features.auth.login

import android.os.Bundle
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.utils.PhoneNumberHelper
import com.appetiser.moviesearch.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.models.user.User
import com.google.i18n.phonenumbers.PhoneNumberUtil
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val resourceManager: ResourceManager,
    private val phoneNumberHelper: PhoneNumberHelper
) : BaseViewModel() {

    private var email: String = ""
    private var phone: String = ""

    private val countryNameCode by lazy {
        resourceManager.getDeviceCountryNameCode()
    }

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    private val _state by lazy {
        PublishSubject.create<LoginState>()
    }

    val state: Observable<LoginState> = _state

    fun setUsername(email: String, phone: String) {
        this.email = email
        this.phone = phone

        val username = if (email.isNotEmpty()) {
            email
        } else {
            phoneNumberHelper
                .getInternationalFormattedPhoneNumber(phone, countryNameCode, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
                .replace("-", " ")
        }

        if (username.isEmpty()) return

        _state.onNext(
            LoginState.GetUsername(username)
        )
    }

    fun login(username: String, password: String) {
        repository
            .loginWithEmail(username, password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LoginState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(LoginState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(LoginState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = { session ->
                    handleLoginResult(session.user)
                },
                onError = {
                    _state.onNext(LoginState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleLoginResult(user: User) {
        if (user.id.isEmpty()) return

        if (!user.verified) {
            _state.onNext(
                LoginState.UserNotVerified(
                    user,
                    user.email,
                    user.phoneNumber
                )
            )
            return
        }

        when {
            !user.hasFullName() -> {
                // User has no first and last name.
                _state.onNext(
                    LoginState.NoFullName
                )
            }
            !user.hasProfilePhoto() -> {
                // User has no profile photo.
                _state.onNext(
                    LoginState.NoProfilePhoto
                )
            }
            else -> {
                // User has all required details.
                _state.onNext(
                    LoginState.LoginSuccess(user)
                )
            }
        }
    }
}
