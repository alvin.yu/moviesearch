package com.appetiser.moviesearch.features.auth.register.details

import com.appetiser.module.domain.models.user.User

sealed class InputNameState {

    data class UserDetailsUpdated(val user: User) : InputNameState()

    data class Error(val throwable: Throwable) : InputNameState()

    data class PreFillName(val fullName: String) : InputNameState()

    object ShowProgressLoading : InputNameState()

    object HideProgressLoading : InputNameState()
}
