package com.appetiser.moviesearch.features.auth.forgotpassword.verification

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentForgotPasswordVerificationCodeBinding
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class ForgotPasswordVerificationFragment : BaseViewModelFragment<FragmentForgotPasswordVerificationCodeBinding, ForgotPasswordVerificationViewModel>() {

    private val args by navArgs<ForgotPasswordVerificationFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_forgot_password_verification_code

    override fun canBack(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupToolbar()
        setupViewModels()
        observeInputViews()

        viewModel.setUsername(args.username)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun setupViews() {
        binding.inputCode.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    viewModel.sendToken(binding.inputCode.text.toString())
                    true
                } else {
                    false
                }
            }
        }

        binding.btnResend.ninjaTap {
            viewModel.resendCode()
        }.addTo(disposables)
    }

    private fun observeInputViews() {
        binding.inputCode.textChangeEvents()
            .skipInitialValue()
            .observeOn(scheduler.ui())
            .map { it.text }
            .map {
                it.isNotEmpty() && it.length >= 5
            }
            .subscribeBy(onNext = {
                if (it) {
                    viewModel.sendToken(binding.inputCode.text.toString())
                }
            }, onError = {
                Timber.e(it)
            }).apply { disposables.add(this) }
    }

    private fun setupViewModels() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: ForgotPasswordVerificationState) {
        when (state) {
            is ForgotPasswordVerificationState.GetUsername -> {
                binding.txtUsername.text = state.username
            }
            is ForgotPasswordVerificationState.ResendTokenSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.resent_code_to_email)
                )
            }
            is ForgotPasswordVerificationState.ForgotPasswordSuccess -> {
                navigate(
                    ForgotPasswordVerificationFragmentDirections.actionForgotPasswordVerificationFragmentToNewPasswordFragment(
                        state.username,
                        state.token
                    )
                )
            }
            is ForgotPasswordVerificationState.Error -> {
                requireActivity()
                    .toast(state.throwable.getThrowableError())
            }
            is ForgotPasswordVerificationState.ShowProgressLoading -> {
                binding.loading setVisible true
            }
            is ForgotPasswordVerificationState.HideProgressLoading -> {
                binding.loading setVisible false
            }
            is ForgotPasswordVerificationState.ResendTimer -> {
                with(binding.btnResend) {
                    if (isEnabled) {
                        isEnabled = false
                    }
                    text = state.time.toString()
                }
            }

            is ForgotPasswordVerificationState.ResendTimerCompleted -> {
                with(binding.btnResend) {
                    isEnabled = true
                    text = getString(R.string.resend)
                }
            }

            is ForgotPasswordVerificationState.ShowResendProgressLoading -> {
                binding.progress.visible()
            }
            is ForgotPasswordVerificationState.HideResendProgressLoading -> {
                binding.progress.gone()
            }
        }
    }
}
