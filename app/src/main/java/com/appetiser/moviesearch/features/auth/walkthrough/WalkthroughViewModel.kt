package com.appetiser.moviesearch.features.auth.walkthrough

import android.os.Bundle
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.utils.PAGE_INITIAL
import com.appetiser.moviesearch.utils.PAGE_STEP_4_POSITION
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class WalkthroughViewModel @Inject constructor() : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<WalkthroughState>()
    }

    val state: Observable<WalkthroughState> = _state

    private var currentPage = PAGE_INITIAL

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadPage() {
        _state
            .onNext(
                WalkthroughState.SelectInitialPage(
                    currentPage
                )
            )

        if (currentPage == PAGE_STEP_4_POSITION) {
            _state.onNext(WalkthroughState.ShowStep4Buttons)
        }
    }

    fun onPageSelected(pagePosition: Int) {
        if (pagePosition == currentPage) return

        when {
            pagePosition == PAGE_STEP_4_POSITION -> {
                _state
                    .onNext(
                        WalkthroughState.UpdatePageIndicator(
                            currentPage,
                            pagePosition
                        )
                    )

                // User reaches step 4
                _state.onNext(WalkthroughState.ShowStep4Buttons)
            }
            pagePosition != PAGE_STEP_4_POSITION && currentPage == PAGE_STEP_4_POSITION -> {
                _state
                    .onNext(
                        WalkthroughState.UpdatePageIndicator(
                            currentPage,
                            pagePosition
                        )
                    )

                // User went back to step 3
                _state.onNext(WalkthroughState.HideStep4Buttons)
            }
            else -> {
                _state
                    .onNext(
                        WalkthroughState.UpdatePageIndicator(
                            currentPage,
                            pagePosition
                        )
                    )
            }
        }

        currentPage = pagePosition
    }
}
