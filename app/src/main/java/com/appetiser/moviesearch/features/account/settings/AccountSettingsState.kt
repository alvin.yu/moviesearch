package com.appetiser.moviesearch.features.account.settings

sealed class AccountSettingsState {

    object NavigateToWalkthroughScreen : AccountSettingsState()

    object ShowLoading : AccountSettingsState()

    object HideLoading : AccountSettingsState()

    data class ShowEmailChangeSuccess(val email: String) : AccountSettingsState()

    data class ShowPhoneChangeSuccess(val phoneNumber: String) : AccountSettingsState()

    object ShowChangePasswordSuccess : AccountSettingsState()

    data class DisplayDetails(
        val email: String,
        val phoneNumber: String
    ) : AccountSettingsState()

    object ShowPrimaryPhoneNumberView : AccountSettingsState()
    object ShowPrimaryEmailView : AccountSettingsState()

    object PrimaryEmail : AccountSettingsState()
    object PrimaryNotEmail : AccountSettingsState()

    object PrimaryPhoneNumber : AccountSettingsState()
    object PrimaryNotPhoneNumber : AccountSettingsState()

    object DeleteAccountUsingPassword : AccountSettingsState()
    object DeleteAccountUsingVerificationCode : AccountSettingsState()

    data class Error(val message: String) : AccountSettingsState()
}
