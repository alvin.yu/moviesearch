package com.appetiser.moviesearch.features.auth.register.profile

import android.os.Bundle
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.user.UserRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class UploadPhotoViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val userRepository: UserRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    private val _state by lazy {
        PublishSubject.create<UploadPhotoState>()
    }

    val state: Observable<UploadPhotoState> = _state

    fun uploadPhoto(filePath: List<String>) {
        if (filePath.isNotEmpty()) {
            val imagePath = filePath[0]
            resourceManager.scaleDownImage(imagePath)
                .flatMap {
                    userRepository
                        .uploadPhoto(
                            if (it.first.isNotEmpty()) it.first else it.second
                        )
                }
                .flatMap {
                    authRepository
                        .onBoardingCompleted()
                }
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doOnSubscribe {
                    _state.onNext(UploadPhotoState.ShowProgressLoading)
                }
                .doOnSuccess {
                    _state.onNext(UploadPhotoState.HideProgressLoading)
                }
                .doOnError {
                    _state.onNext(UploadPhotoState.HideProgressLoading)
                }
                .subscribeBy(
                    onSuccess = {
                        _state.onNext(UploadPhotoState.ShowSubscriptionScreen)
                    },
                    onError = {
                        _state.onNext(UploadPhotoState.ErrorUploadPhoto(it))
                    }
                )
                .apply { disposables.add(this) }
        }
    }

    fun onBoardingComplete() {
        authRepository
            .onBoardingCompleted()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    _state.onNext(UploadPhotoState.ShowSubscriptionScreen)
                },
                onError = {
                    _state.onNext(UploadPhotoState.ErrorUploadPhoto(it))
                }
            )
            .addTo(disposables)
    }
}
