package com.appetiser.moviesearch.features.account

sealed class AccountState {
    object ShowLoading : AccountState()

    object HideLoading : AccountState()

    data class Error(val message: String) : AccountState()
}
