package com.appetiser.moviesearch.features.account.about

import android.os.Bundle
import android.view.View
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.moviesearch.BuildConfig
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseFragment
import com.appetiser.moviesearch.databinding.FragmentAboutBinding
import io.reactivex.rxkotlin.addTo

class AboutFragment : BaseFragment<FragmentAboutBinding>() {

    companion object {
        const val TERMS_AND_CONDITION_URL = "https://baseplate-api.appetiserdev.tech"
        const val PRIVACY_POLICY_URL = "https://appetiser.com.au"
    }

    override fun getLayoutId(): Int = R.layout.fragment_about

    override fun canBack() = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()

        binding.btnTermsAndConditions.ninjaTap {
            navigate(
                AboutFragmentDirections.actionAboutFragmentToWebViewFragment(
                    TERMS_AND_CONDITION_URL
                )
            )
        }.addTo(disposables)

        binding.btnPrivacyPolicy.ninjaTap {
            navigate(
                AboutFragmentDirections.actionAboutFragmentToWebViewFragment(
                    PRIVACY_POLICY_URL
                )
            )
        }.addTo(disposables)

        binding.txtVersion.text = getString(R.string.version_format, BuildConfig.VERSION_NAME)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(
            binding.toolbar.toolbarView,
            getString(R.string.about)
        )
    }
}
