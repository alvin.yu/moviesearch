package com.appetiser.moviesearch.features.auth.landing

import android.os.Bundle
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.utils.PhoneNumberHelper
import com.appetiser.moviesearch.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.models.auth.UsernameType
import com.appetiser.module.network.base.response.BaseErrorResponse
import com.appetiser.module.network.features.auth.models.UserNameCheckErrorMeta
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class LandingViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val phoneNumberHelper: PhoneNumberHelper,
    private val resourceManager: ResourceManager,
    private val gson: Gson
) : BaseViewModel() {

    companion object {
        const val FACEBOOK = "facebook"
        const val GOOGLE = "google"
        const val TRADITIONAL = "traditional"
    }

    private val _state by lazy {
        PublishSubject.create<LandingState>()
    }

    val state: Observable<LandingState> = _state

    private val countryNameCode by lazy {
        resourceManager.getDeviceCountryNameCode()
    }

    private var username: String = ""

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadLanding() {
        if (username.isEmpty()) return

        // Useful when navigating away and back to this screen.
        onUsernameTextChanged(username)
    }

    fun onUsernameTextChanged(username: CharSequence) {
        val isDigitsOnly = phoneNumberHelper.isNumberDigitsOnly(username)

        if (isDigitsOnly && countryNameCode.isNotEmpty()) {
            val countryCodeAndNumber =
                phoneNumberHelper.getCountryCodeAndNumber(
                    username.toString(),
                    countryNameCode
                )

            if (countryCodeAndNumber.first.isNotEmpty()) {
                _state.onNext(
                    LandingState.SetInputModeMobile(
                        countryCodeAndNumber.first,
                        countryCodeAndNumber.second
                    )
                )
            } else {
                _state.onNext(
                    LandingState.SetInputModeFreeText(countryCodeAndNumber.second)
                )
            }
        } else {
            _state.onNext(
                LandingState.SetInputModeFreeText(username.toString())
            )
        }

        if (username.isNotEmpty()) {
            _state.onNext(LandingState.EnableContinueButton)
        } else {
            _state.onNext(LandingState.DisableContinueButton)
        }
    }

    fun onContinueClick(username: String) {
        this.username = username

        authRepository
            .checkUsername(username)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LandingState.ShowLoading(TRADITIONAL))
            }
            .doOnSuccess {
                _state.onNext(LandingState.HideLoading)
            }
            .doOnError {
                _state.onNext(LandingState.HideLoading)
            }
            .subscribeBy(
                onSuccess = { usernameCheck ->
                    when (usernameCheck.userNameType) {
                        UsernameType.Email -> {
                            _state.onNext(
                                LandingState.EmailUsernameExists(
                                    username
                                )
                            )
                        }
                        UsernameType.Phone -> {
                            _state.onNext(
                                LandingState.MobileUsernameExists(
                                    username
                                )
                            )
                        }
                    }
                },
                onError = { error ->
                    Timber.e(error)
                    when (error) {
                        is HttpException -> {
                            if (error.code() == 404) {
                                handleCheckUsername404Response(error, username)
                            } else if (error.code() == 401) {
                                handleCheckUsername401Response(error)
                            } else {
                                _state.onNext(LandingState.Error(error))
                            }
                        }
                        else -> {
                            _state.onNext(LandingState.Error(error))
                        }
                    }
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleCheckUsername404Response(error: HttpException, username: String) {
        val responseBodyString = error.response()?.errorBody()?.string()

        if (responseBodyString.isNullOrEmpty()) {
            _state.onNext(LandingState.Error(error))
            return
        }

        val errorMeta =
            gson.fromJson(
                responseBodyString,
                object : TypeToken<BaseErrorResponse<UserNameCheckErrorMeta>>() {}.type
            ) as BaseErrorResponse<UserNameCheckErrorMeta>
        val usernameTypeString = errorMeta.meta.usernameType

        when (UsernameType.getUsernameTypeFromString(usernameTypeString)) {
            UsernameType.Email -> {
                _state.onNext(
                    LandingState.EmailUsernameDoesNotExists(
                        username
                    )
                )
            }
            UsernameType.Phone -> {
                _state.onNext(
                    LandingState.MobileUsernameDoesNotExists(
                        username
                    )
                )
            }
        }
    }

    private fun handleCheckUsername401Response(error: HttpException) {
        val responseBodyString = error.response()?.errorBody()?.string()

        if (responseBodyString.isNullOrEmpty()) {
            _state.onNext(LandingState.Error(error))
            return
        }

        val errorMeta =
            gson.fromJson(
                responseBodyString,
                object : TypeToken<BaseErrorResponse<UserNameCheckErrorMeta>>() {}.type
            ) as BaseErrorResponse<UserNameCheckErrorMeta>

        _state.onNext(
            LandingState.WrongPrimaryAccount(
                errorMeta.message
            )
        )
    }

    fun onFacebookLogin(facebookAccessToken: String) {
        socialLogin(facebookAccessToken, FACEBOOK)
    }

    fun onGoogleSignIn(googleAccessToken: String) {
        socialLogin(googleAccessToken, GOOGLE)
    }

    private fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ) {
        authRepository
            .socialLogin(accessToken, accessTokenProvider)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LandingState.ShowLoading(accessTokenProvider))
            }
            .doOnSuccess {
                _state.onNext(LandingState.HideLoading)
            }
            .doOnError {
                _state.onNext(LandingState.HideLoading)
            }
            .subscribeBy(
                onSuccess = { session ->
                    Timber.tag("socialLogin").d(gson.toJson(session))
                    if (session.user.hasFullName()) {
                        _state.onNext(LandingState.SocialLoginSuccess)
                    } else {
                        _state.onNext(LandingState.SocialLoginSuccessButNotOnboarded)
                    }
                },
                onError = {
                    Timber.e(it)
                    _state.onNext(LandingState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }

    fun generateOTP(phone: String) {
        authRepository
            .generateOtp(
                phoneNumber = phone
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LandingState.ShowLoading(TRADITIONAL))
            }
            .doOnSuccess {
                _state.onNext(LandingState.HideLoading)
            }
            .doOnError {
                _state.onNext(LandingState.HideLoading)
            }
            .subscribeBy(
                onSuccess = {
                    if (it) {
                        _state
                            .onNext(
                                LandingState.MobileRegistrationGenerateOTPSuccess(
                                    phone
                                )
                            )
                    }
                },
                onError = { throwable ->
                    _state
                        .onNext(
                            LandingState.Error(
                                throwable
                            )
                        )
                }
            )
            .addTo(disposables)
    }
}
