package com.appetiser.moviesearch.features.auth.landing

sealed class LandingState {
    class ShowLoading(val loginType: String) : LandingState()

    object HideLoading : LandingState()

    /**
     * User login success and filled with onboarding details.
     */
    object SocialLoginSuccess : LandingState()

    /**
     * User login success but not filled with onboarding details.
     * This can happend when backend cannot fetch user details from 3rd party provider.
     */
    object SocialLoginSuccessButNotOnboarded : LandingState()

    object EnableContinueButton : LandingState()

    object DisableContinueButton : LandingState()

    data class Error(val throwable: Throwable) : LandingState()

    data class WrongPrimaryAccount(val message: String) : LandingState()

    data class EmailUsernameExists(
        val email: String
    ) : LandingState()

    data class MobileUsernameExists(
        val mobile: String
    ) : LandingState()

    data class EmailUsernameDoesNotExists(
        val email: String
    ) : LandingState()

    data class MobileUsernameDoesNotExists(
        val mobile: String
    ) : LandingState()

    data class MobileRegistrationGenerateOTPSuccess(
        val mobile: String
    ) : LandingState()

    data class SetInputModeMobile(
        val countryCode: String,
        val number: String
    ) : LandingState()

    data class SetInputModeFreeText(
        val input: String
    ) : LandingState()
}
