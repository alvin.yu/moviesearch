package com.appetiser.moviesearch.features.auth.walkthrough

sealed class WalkthroughState {
    data class UpdatePageIndicator(
        val previousPage: Int,
        val currentPage: Int
    ) : WalkthroughState()

    object ShowStep4Buttons : WalkthroughState()

    object HideStep4Buttons : WalkthroughState()

    data class SelectInitialPage(
        val currentPage: Int
    ) : WalkthroughState()
}
