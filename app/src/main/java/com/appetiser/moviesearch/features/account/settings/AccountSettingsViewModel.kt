package com.appetiser.moviesearch.features.account.settings

import android.os.Bundle
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.models.user.PrimaryUserNameType
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.utils.ResourceManager
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class AccountSettingsViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val userRepository: UserRepository,
    private val sessionRepository: SessionRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<AccountSettingsState>()
    }

    val state: Observable<AccountSettingsState> = _state

    private var isFirstOpen = true

    private lateinit var primaryUserNameType: PrimaryUserNameType

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadAccountSettings() {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    handleSession(session)
                },
                onError = {}
            )
            .addTo(disposables)
    }

    private fun handleSession(session: Session) {
        _state
            .onNext(
                AccountSettingsState.DisplayDetails(
                    if (session.user.email.isNotEmpty()) {
                        session.user.email
                    } else {
                        resourceManager.getString(R.string.na)
                    },
                    if (session.user.phoneNumber.isNotEmpty()) {
                        "+${session.user.phoneNumber}"
                    } else {
                        resourceManager.getString(R.string.na)
                    }
                )
            )

        primaryUserNameType = session.user.primaryUserNameType
        when (primaryUserNameType) {
            PrimaryUserNameType.EMAIL -> {
                _state
                    .onNext(
                        AccountSettingsState.ShowPrimaryEmailView
                    )
            }
            PrimaryUserNameType.PHONE_NUMBER -> {
                _state
                    .onNext(
                        AccountSettingsState.ShowPrimaryPhoneNumberView
                    )
            }
        }
    }

    fun setFromChangeEmail(fromChangeEmail: Boolean) {
        if (fromChangeEmail) {
            userRepository.getUser()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeBy(
                    onSuccess = {
                        _state
                            .onNext(
                                AccountSettingsState.ShowEmailChangeSuccess(it.email)
                            )
                    },
                    onError = {
                    }
                )
                .addTo(disposables)
        }
    }

    fun setFromChangePhone(fromChangePhone: Boolean) {
        if (fromChangePhone) {
            userRepository.getUser()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeBy(
                    onSuccess = {
                        _state
                            .onNext(
                                AccountSettingsState.ShowPhoneChangeSuccess(it.phoneNumber)
                            )
                    },
                    onError = {
                    }
                )
                .addTo(disposables)
        }
    }

    fun setFromChangePassword(fromChangePassword: Boolean) {
        if (fromChangePassword) {
            _state.onNext(
                AccountSettingsState.ShowChangePasswordSuccess
            )
        }
    }

    fun logout() {
        authRepository
            .logout()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(AccountSettingsState.ShowLoading)
            }
            .doOnComplete {
                _state.onNext(AccountSettingsState.HideLoading)
            }
            .doOnError {
                _state.onNext(AccountSettingsState.HideLoading)
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            AccountSettingsState.NavigateToWalkthroughScreen
                        )
                },
                onError = {
                    showGenericError()
                }
            )
            .addTo(disposables)
    }

    private fun showGenericError() {
        _state
            .onNext(
                AccountSettingsState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    fun changeMobileNumber() {
        if (primaryUserNameType == PrimaryUserNameType.PHONE_NUMBER) {
            _state.onNext(AccountSettingsState.PrimaryPhoneNumber)
        } else {
            _state.onNext(AccountSettingsState.PrimaryNotPhoneNumber)
        }
    }

    fun changeEmail() {
        if (primaryUserNameType == PrimaryUserNameType.EMAIL) {
            _state.onNext(AccountSettingsState.PrimaryEmail)
        } else {
            _state.onNext(AccountSettingsState.PrimaryNotEmail)
        }
    }

    fun deleteAccount() {
        if (primaryUserNameType == PrimaryUserNameType.EMAIL) {
            _state.onNext(AccountSettingsState.DeleteAccountUsingPassword)
        } else {
            _state.onNext(AccountSettingsState.DeleteAccountUsingVerificationCode)
        }
    }
}
