package com.appetiser.moviesearch.features.account.primaryemail

import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.NavOptions
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.moviesearch.MainGraphDirections
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentPrimaryEmailPasswordBinding
import com.appetiser.moviesearch.features.auth.FacebookLoginManager
import com.appetiser.moviesearch.features.auth.GoogleSignInManager
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class PrimaryEmailPasswordFragment : BaseViewModelFragment<FragmentPrimaryEmailPasswordBinding, PrimaryEmailPasswordViewModel>() {

    private val args by navArgs<PrimaryEmailPasswordFragmentArgs>()

    override fun canBack(): Boolean = true

    override fun getLayoutId(): Int = R.layout.fragment_primary_email_password

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.passwordMaxLength = PASSWORD_MAX_LENGTH

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.setScreenFlow(args.type)
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: PrimaryEmailPasswordState) {
        when (state) {
            PrimaryEmailPasswordState.InitializeChangeEmail -> {
                binding.txtTitle.text = getString(R.string.verify_password)
                binding.txtNewPasswordLabel.text = getString(R.string.verify_password_for_email_description)
            }

            PrimaryEmailPasswordState.InitializeChangePhoneNumber -> {
                binding.txtTitle.text = getString(R.string.verify_password)
                binding.txtNewPasswordLabel.text = getString(R.string.verify_password_for_mobile_description)
            }

            PrimaryEmailPasswordState.InitializeChangePassword -> {
                binding.txtTitle.text = getString(R.string.verify_password)
                binding.txtNewPasswordLabel.text = getString(R.string.verify_password_for_current_password_description)
            }

            PrimaryEmailPasswordState.InitializeDeleteAccount -> {
                binding.txtTitle.text = getString(R.string.verify_password)
                binding.txtNewPasswordLabel.text = getString(R.string.verify_password_for_delete_account_description)
            }

            PrimaryEmailPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }

            PrimaryEmailPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }

            PrimaryEmailPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }

            PrimaryEmailPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }

            PrimaryEmailPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            PrimaryEmailPasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            PrimaryEmailPasswordState.ShowLoading -> {
                binding.loading setVisible true
            }
            PrimaryEmailPasswordState.HideLoading -> {
                binding.loading setVisible false
            }
            PrimaryEmailPasswordState.InvalidPassword -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.invalid_password)
            }
            is PrimaryEmailPasswordState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }

            is PrimaryEmailPasswordState.PhonePasswordVerificationSuccess -> {
                navigate(PrimaryEmailPasswordFragmentDirections.actionAccountPrimaryEmailFragmentToChangePhoneGraph(state.verificationToken))
            }

            is PrimaryEmailPasswordState.EmailPasswordVerificationSuccess -> {
                navigate(PrimaryEmailPasswordFragmentDirections.actionAccountPrimaryEmailFragmentToChangeEmailNewEmailFragment(state.verificationToken))
            }

            is PrimaryEmailPasswordState.CurrentPasswordVerificationSuccess -> {
                navigate(PrimaryEmailPasswordFragmentDirections.actionAccountPrimaryEmailFragmentToChangeNewPasswordFragment(binding.inputPassword.text.toString().trim(), state.verificationToken))
            }

            is PrimaryEmailPasswordState.NavigateToWalkthroughScreen -> {
                logoutSocialLogins()

                val navOptions = NavOptions
                    .Builder()
                    .setEnterAnim(R.anim.anim_slide_in_bottom)
                    .setExitAnim(R.anim.anim_slide_out_top)
                    .build()

                navigate(
                    MainGraphDirections
                        .actionGlobalToWalkthroughGraph(),
                    navOptions
                )
            }
        }
    }

    private fun setupViews() {
        binding.inputLayoutPassword.apply {
            setEndIconOnClickListener {
                editText?.let { et ->
                    // Store the current cursor position
                    val selection = et.selectionEnd

                    if (et.transformationMethod != null &&
                        et.transformationMethod is PasswordTransformationMethod
                    ) {
                        et.transformationMethod = null
                    } else {
                        et.transformationMethod = CustomPasswordTransformation.getInstance()
                    }

                    // And restore the cursor position
                    if (selection >= 0) {
                        et.setSelection(selection)
                    }
                }
            }

            binding.inputPassword.apply {
                transformationMethod = CustomPasswordTransformation.getInstance()
            }

            with(binding.inputPassword) {
                textChanges()
                    .skipInitialValue()
                    .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                    .subscribeBy(
                        onNext = { text ->
                            viewModel.onPasswordTextChanged(text.toString())
                        },
                        onError = {
                            Timber.e(it)
                        }
                    )
                    .addTo(disposables)

                editorActionEvents()
                    .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                    .subscribeOn(scheduler.ui())
                    .subscribeBy(
                        onNext = {
                            if (it.actionId == EditorInfo.IME_ACTION_GO) {
                                binding.inputLayoutPassword.error = ""
                                binding.root.hideKeyboard()
                                viewModel
                                    .verifyPassword(
                                        text.toString().trim()
                                    )
                            }
                        },
                        onError = {
                            Timber.e(it)
                        }
                    )
                    .addTo(disposables)
            }

            binding
                .btnContinue
                .ninjaTap {
                    binding.inputLayoutPassword.error = ""
                    binding.root.hideKeyboard()
                    viewModel
                        .verifyPassword(
                            binding.inputPassword.text.toString().trim()
                        )
                }
                .addTo(disposables)
        }
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun logoutSocialLogins() {
        // Sign out google if it's signed in.
        if (GoogleSignInManager.isSignedIn(requireContext())) {
            GoogleSignInManager(this)
                .run {
                    signOut()
                }
        }

        // Logout facebook if it's logged in.
        if (FacebookLoginManager.isLoggedIn()) {
            FacebookLoginManager.logout()
        }
    }
}
