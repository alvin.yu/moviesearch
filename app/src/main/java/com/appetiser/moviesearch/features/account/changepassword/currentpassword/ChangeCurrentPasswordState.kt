package com.appetiser.moviesearch.features.account.changepassword.currentpassword

sealed class ChangeCurrentPasswordState {

    object EnableButton : ChangeCurrentPasswordState()

    object DisableButton : ChangeCurrentPasswordState()

    object ShowLoading : ChangeCurrentPasswordState()

    object HideLoading : ChangeCurrentPasswordState()

    object PasswordExceedsMaxLength : ChangeCurrentPasswordState()

    object PasswordBelowMinLength : ChangeCurrentPasswordState()

    object InvalidPassword : ChangeCurrentPasswordState()

    data class VerificationSuccess(
        val oldPassword: String,
        val verificationToken: String
    ) : ChangeCurrentPasswordState()

    data class Error(val message: String) : ChangeCurrentPasswordState()
}
