package com.appetiser.moviesearch.features.auth.welcome

import android.graphics.Color
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.navigation.fragment.FragmentNavigatorExtras
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.extensions.spannableString
import com.appetiser.module.common.extensions.toast
import com.appetiser.moviesearch.BuildConfig
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseFragment
import com.appetiser.moviesearch.databinding.FragmentWelcomeBinding
import io.reactivex.rxkotlin.addTo

class WelcomeFragment : BaseFragment<FragmentWelcomeBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_welcome

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        setupPrivacy()

        binding
            .btnContinue
            .ninjaTap {
                navigateToLandingScreen()
            }
            .addTo(disposables)

        binding.txtVersion.text = getString(R.string.version_format, BuildConfig.VERSION_NAME)
    }

    private fun navigateToLandingScreen() {
        navigate(
            WelcomeFragmentDirections.actionWelcomeFragmentToLandingFragment(),
            FragmentNavigatorExtras(
                binding.imgLanding to "imgLanding",
                binding.txtAppTitle to "txtAppTitle"
            )
        )
    }

    private fun setupPrivacy() {
        binding.txtPrivacy.movementMethod = LinkMovementMethod.getInstance()
        binding.txtPrivacy.text = getString(R.string.auth_privacy_policy)
            .spannableString(
                requireContext(),
                null,
                R.font.inter_semibold,
                Color.WHITE,
                "Terms of Service",
                "Privacy Policy",
                clickable = {
                    requireActivity()
                        .toast("Clicked $it")
                }
            )
    }
}
