package com.appetiser.moviesearch.features.home

import android.os.Bundle
import android.text.format.DateFormat
import com.appetiser.module.data.features.movie.MovieRepository
import com.appetiser.module.domain.models.movie.Movie
import com.appetiser.module.local.features.movie.MovieLocalSourceImpl.Companion.PREF_LAST_USER_VISIT_DEFAULT_VALUE
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.moviesearch.ext.getThrowableError
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class MovieViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<MovieListState>()
    }
    val state: Observable<MovieListState> = _state

    fun loadMovie(movieId: Long) {
        movieRepository
            .getMovie(movieId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { movie ->
                    _state.onNext(MovieListState.DisplayMovie(movie))
                },
                onError = {
                }
            )
            .addTo(disposables)
    }

    fun loadCachedMovies() {
        movieRepository
            .getMovieList()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                updateCachedMovies()
                _state.onNext(MovieListState.ShowLoading)
            }
            .subscribeBy(
                onSuccess = { movieList ->
                    _state.onNext(MovieListState.DisplayMovieList(movieList))
                    _state.onNext(MovieListState.HideLoading)
                },
                onError = {
                    _state.onNext(MovieListState.HideLoading)
                    Timber.e("${it.localizedMessage}")
                }
            )
            .addTo(disposables)
    }

    private fun updateCachedMovies() {
        movieRepository
            .getMovieList()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { movieList ->
                    saveMovieList(movieList)
                    Timber.e("${movieList.size}")
                },
                onError = {
                    Timber.e("${it.localizedMessage}")
                }
            )
            .addTo(disposables)
    }

    private fun saveMovieList(movieList: List<Movie>) {
        movieRepository
            .saveMovieList(movieList)
            .subscribeOn(schedulers.io())
            .subscribeBy(
                onSuccess = {
                    Timber.e("$it")
                },
                onError = {
                    Timber.e("${it.localizedMessage}")
                }
            )
            .addTo(disposables)
    }

    fun loadLastUserVisit() {
        movieRepository
            .getLastUserVisit()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    val formattedDate = when (it) {
                        PREF_LAST_USER_VISIT_DEFAULT_VALUE -> ""
                        else -> formatTimestampToDatetime(it)
                    }
                    _state.onNext(MovieListState.DisplayLastUserVisit(formattedDate))
                },
                onError = {
                    Timber.e("${it.getThrowableError()}")
                }
            )
            .addTo(disposables)
    }

    private fun formatTimestampToDatetime(timestamp: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timestamp
        return DateFormat.format("yyyy MMM dd HH:mm:ss", calendar).toString()
    }

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit
}
