package com.appetiser.moviesearch.features.account.changeemail.verifycode

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentChangeEmailVerifyCodeBinding
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeEmailVerifyCodeFragment : BaseViewModelFragment<FragmentChangeEmailVerifyCodeBinding, ChangeEmailVerifyCodeViewModel>() {

    private val args by navArgs<ChangeEmailVerifyCodeFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_change_email_verify_code

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.setEmailAndVerificationToken(
            args.email,
            args.verificationToken
        )
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangeEmailVerifyCodeState) {
        when (state) {
            is ChangeEmailVerifyCodeState.DisplayEmail -> {
                binding
                    .txtEmail
                    .text = state.email
            }
            ChangeEmailVerifyCodeState.ShowLoading -> {
                binding.root.hideKeyboard()
            }
            ChangeEmailVerifyCodeState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangeEmailVerifyCodeState.HideLoading -> {
                binding.loading setVisible false
            }
            is ChangeEmailVerifyCodeState.ResendCodeSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.resent_code_to_email)
                )
            }
            is ChangeEmailVerifyCodeState.VerificationSuccess -> {
                navigate(
                    ChangeEmailVerifyCodeFragmentDirections.actionChangeEmailVerifyCodeFragmentToAccountSettingsGraph()
                )
            }
            is ChangeEmailVerifyCodeState.InvalidVerificationCode -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
            is ChangeEmailVerifyCodeState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }

            is ChangeEmailVerifyCodeState.ResendTimer -> {
                with(binding.btnResend) {
                    if (isEnabled) {
                        isEnabled = false
                    }
                    text = state.time.toString()
                }
            }

            is ChangeEmailVerifyCodeState.ResendTimerCompleted -> {
                with(binding.btnResend) {
                    isEnabled = true
                    text = getString(R.string.resend)
                }
            }

            is ChangeEmailVerifyCodeState.ShowResendProgressLoading -> {
                binding.progress.visible()
            }
            is ChangeEmailVerifyCodeState.HideResendProgressLoading -> {
                binding.progress.gone()
            }
        }
    }

    private fun setupViews() {
        binding
            .inputCode
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onCodeTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnResend
            .ninjaTap {
                viewModel
                    .resendCode()
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }
}
