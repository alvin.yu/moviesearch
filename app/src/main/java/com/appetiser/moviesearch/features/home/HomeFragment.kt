package com.appetiser.moviesearch.features.home

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import com.appetiser.module.common.extensions.setVisible
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentHomeBinding
import com.appetiser.moviesearch.utils.TwoPaneOnBackPressedCallback
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class HomeFragment() : BaseViewModelFragment<FragmentHomeBinding, MovieViewModel>(), Parcelable {

    override fun canBack(): Boolean = true

    private val adapter: MovieListAdapter by lazy {
        MovieListAdapter(MovieListener { movie ->
            openDetails(
                SelectAMovieFragmentDirections.actionSelectAMovieFragmentToMovieDetailFragment(
                    movie.trackId
                )
            )
        })
    }

    constructor(parcel: Parcel) : this() {
    }

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupVmObservers()

        viewModel.loadCachedMovies()

//         Connect the SlidingPaneLayout to the system back button.
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            TwoPaneOnBackPressedCallback(binding.slidingPaneLayout)
        )
    }

    override fun onStart() {
        super.onStart()

        viewModel.loadLastUserVisit()
    }

    private fun setupViews() {
        binding.movieListPane.adapter = adapter
    }

    private fun setupVmObservers() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                },
                onError = {
                })
            .addTo(disposables)
    }

    private fun handleState(state: MovieListState) {
        when (state) {

            is MovieListState.ShowLoading -> {
                binding.loading setVisible true
            }

            is MovieListState.HideLoading -> {
                binding.loading setVisible false
            }

            is MovieListState.DisplayMovieList -> {
                adapter.addHeaderAndSubmitList(state.movieList)
            }

            is MovieListState.DisplayLastUserVisit -> {
                adapter.updateHeader(
                    when (state.formattedDate) {
                        "" -> getString(R.string.first_visit)
                        else -> getString(
                            R.string.last_user_visit_timestamp_format,
                            state.formattedDate
                        )
                    }
                )
            }

            else -> Timber.e("Unhandled state: $state")
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HomeFragment> {
        override fun createFromParcel(parcel: Parcel): HomeFragment {
            return HomeFragment(parcel)
        }

        override fun newArray(size: Int): Array<HomeFragment?> {
            return arrayOfNulls(size)
        }
    }

    // A method on the Fragment that owns the SlidingPaneLayout,
    // called by the adapter when an item is selected.
    private fun openDetails(navDirections: NavDirections) {
        // Assume the NavHostFragment is added with the +id/detail_container.
        val navHostFragment = childFragmentManager.findFragmentById(
            R.id.movie_detail_container
        ) as NavHostFragment
        val navController = navHostFragment.navController
        if (navController.currentDestination != navController.graph.findNode(R.id.selectAMovieFragment)) {
            navController.navigate(MovieDetailFragmentDirections.actionMovieDetailFragmentToSelectAMovieFragment())
        }
        navController.navigate(navDirections)
        binding.slidingPaneLayout.open()
    }
}
