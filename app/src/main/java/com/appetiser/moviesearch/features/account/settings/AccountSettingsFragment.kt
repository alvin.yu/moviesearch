package com.appetiser.moviesearch.features.account.settings

import android.os.Bundle
import android.view.View
import androidx.navigation.NavOptions
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.extensions.setVisible
import com.appetiser.moviesearch.MainGraphDirections
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentAccountSettingsBinding
import com.appetiser.moviesearch.features.account.primaryemail.PrimaryEmailPasswordScreenFlow
import com.appetiser.moviesearch.features.account.primaryphone.PrimaryPhoneVerificationScreenFlow
import com.appetiser.moviesearch.features.auth.FacebookLoginManager
import com.appetiser.moviesearch.features.auth.GoogleSignInManager
import com.appetiser.moviesearch.utils.ViewUtils
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class AccountSettingsFragment : BaseViewModelFragment<FragmentAccountSettingsBinding, AccountSettingsViewModel>() {

    private val args by navArgs<AccountSettingsFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_account_settings

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupViewModel()
        setupViews()

        viewModel.setFromChangeEmail(args.fromChangeEmail)
        viewModel.setFromChangePhone(args.fromChangePhone)
        viewModel.setFromChangePassword(args.fromChangePassword)

        viewModel.loadAccountSettings()
    }

    private fun setupViews() {
        binding
            .btnChangeEmail
            .ninjaTap {
                viewModel.changeEmail()
            }
            .addTo(disposables)

        binding
            .btnChangeMobileNumber
            .ninjaTap {
                viewModel.changeMobileNumber()
            }
            .addTo(disposables)

        binding
            .btnLogout
            .ninjaTap {
                showLogoutConfirmationDialog()
            }
            .addTo(disposables)

        binding
            .btnDeleteAccount
            .ninjaTap {
                showDeleteAccountConfirmationDialog()
            }
            .addTo(disposables)

        binding
            .btnChangePassword
            .ninjaTap {
                navigate(
                    AccountSettingsFragmentDirections.actionAccountSettingsFragmentToChangePasswordGraph()
                )
            }
    }

    private fun showLogoutConfirmationDialog() {
        ViewUtils.showConfirmDialog(
            requireContext(),
            body = getString(R.string.logout_message),
            negativeButtonText = getString(R.string.cancel),
            positiveButtonText = getString(R.string.yes),
            positiveClickListener = {
                viewModel.logout()
            }
        )
    }

    private fun showDeleteAccountConfirmationDialog() {
        ViewUtils.showConfirmDialog(
            requireContext(),
            title = getString(R.string.delete_account_confirmation_title),
            body = getString(R.string.delete_account_confirmation_body),
            negativeButtonText = getString(R.string.cancel),
            positiveButtonText = getString(R.string.yes),
            positiveClickListener = {
                viewModel.deleteAccount()
            }
        )
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
        binding.toolbar.txtToolbarCustomTitle.text = getString(R.string.account_settings)
    }

    private fun setupViewModel() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                },
                onError = { Timber.e("Error $it ") }
            )
            .addTo(disposables)
    }

    private fun handleState(state: AccountSettingsState) {
        when (state) {
            is AccountSettingsState.ShowEmailChangeSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.your_email_has_been_updated)
                )
                binding.txtEmailAddress.text = state.email
            }
            is AccountSettingsState.ShowPhoneChangeSuccess -> {
                val phoneNumber = if (state.phoneNumber.contains("+")) state.phoneNumber
                else getString(R.string.phone_number_format, state.phoneNumber)

                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.your_mobile_number_has_been_updated)
                )

                binding.txtMobileNumber.text = phoneNumber
            }
            is AccountSettingsState.ShowChangePasswordSuccess -> {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.your_password_has_been_updated)
                )
            }
            is AccountSettingsState.ShowLoading -> {
                binding.loading setVisible true
            }
            is AccountSettingsState.HideLoading -> {
                binding.loading setVisible false
            }
            is AccountSettingsState.DisplayDetails -> {
                binding.txtEmailAddress.text = state.email
                binding.txtMobileNumber.text = state.phoneNumber
            }
            is AccountSettingsState.NavigateToWalkthroughScreen -> {
                logoutSocialLogins()

                val navOptions = NavOptions
                    .Builder()
                    .setEnterAnim(R.anim.anim_slide_in_bottom)
                    .setExitAnim(R.anim.anim_slide_out_top)
                    .build()

                navigate(
                    MainGraphDirections
                        .actionGlobalToWalkthroughGraph(),
                    navOptions
                )
            }
            is AccountSettingsState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }

            is AccountSettingsState.PrimaryPhoneNumber -> {
                navigate(
                    AccountSettingsFragmentDirections.actionAccountSettingsFragmentToPrimaryPhoneVerificationFragment(PrimaryPhoneVerificationScreenFlow.CHANGE_PHONE)
                )
            }

            is AccountSettingsState.PrimaryNotPhoneNumber -> {
                navigate(
                    AccountSettingsFragmentDirections.actionAccountSettingsFragmentToAccountPrimaryEmailFragment(PrimaryEmailPasswordScreenFlow.CHANGE_PHONE)
                )
            }

            is AccountSettingsState.PrimaryEmail -> {
                navigate(
                    AccountSettingsFragmentDirections.actionAccountSettingsFragmentToAccountPrimaryEmailFragment(PrimaryEmailPasswordScreenFlow.CHANGE_EMAIL)
                )
            }

            is AccountSettingsState.PrimaryNotEmail -> {
                navigate(
                    AccountSettingsFragmentDirections.actionAccountSettingsFragmentToPrimaryPhoneVerificationFragment(PrimaryPhoneVerificationScreenFlow.CHANGE_EMAIL)
                )
            }

            is AccountSettingsState.ShowPrimaryEmailView -> {
                binding.layoutParent.removeView(binding.layoutEmail)
                binding.layoutParent.removeView(binding.layoutMobileNumber)

                binding.layoutParent.addView(binding.layoutEmail, 1)
                binding.layoutParent.addView(binding.layoutMobileNumber, 2)
            }
            is AccountSettingsState.ShowPrimaryPhoneNumberView -> {
                binding.layoutPassword setVisible false
                binding.layoutParent.removeView(binding.layoutEmail)
                binding.layoutParent.removeView(binding.layoutMobileNumber)

                binding.layoutParent.addView(binding.layoutMobileNumber, 1)
                binding.layoutParent.addView(binding.layoutEmail, 2)
            }

            is AccountSettingsState.DeleteAccountUsingPassword -> {
                navigate(
                    AccountSettingsFragmentDirections.actionAccountSettingsFragmentToAccountPrimaryEmailFragment(
                        PrimaryEmailPasswordScreenFlow.CHANGE_DELETE
                    )
                )
            }

            is AccountSettingsState.DeleteAccountUsingVerificationCode -> {
                navigate(
                    AccountSettingsFragmentDirections.actionAccountSettingsFragmentToPrimaryPhoneVerificationFragment(
                        PrimaryPhoneVerificationScreenFlow.CHANGE_DELETE
                    )
                )
            }
        }
    }

    private fun logoutSocialLogins() {
        // Sign out google if it's signed in.
        if (GoogleSignInManager.isSignedIn(requireContext())) {
            GoogleSignInManager(this)
                .run {
                    signOut()
                }
        }

        // Logout facebook if it's logged in.
        if (FacebookLoginManager.isLoggedIn()) {
            FacebookLoginManager.logout()
        }
    }
}
