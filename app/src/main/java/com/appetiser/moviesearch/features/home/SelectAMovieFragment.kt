package com.appetiser.moviesearch.features.home

import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseFragment
import com.appetiser.moviesearch.databinding.FragmentSelectAMovieBinding

class SelectAMovieFragment : BaseFragment<FragmentSelectAMovieBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_select_a_movie
}
