package com.appetiser.moviesearch.features.auth.register.createpassword

import android.os.Bundle
import com.appetiser.moviesearch.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.utils.PhoneNumberHelper
import com.appetiser.moviesearch.utils.ResourceManager
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH
import com.google.i18n.phonenumbers.PhoneNumberUtil
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class CreatePasswordViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val resourceManager: ResourceManager,
    private val phoneNumberHelper: PhoneNumberHelper
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<CreatePasswordState>()
    }

    val state: Observable<CreatePasswordState> = _state
    private lateinit var email: String
    private lateinit var phone: String

    private val countryNameCode by lazy {
        resourceManager.getDeviceCountryNameCode()
    }

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setUsername(email: String, phone: String) {
        this.email = email
        this.phone = phone

        val username = if (email.isNotEmpty()) {
            email
        } else {
            phoneNumberHelper
                .getInternationalFormattedPhoneNumber(phone, countryNameCode, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
                .replace("-", " ")
        }

        _state.onNext(
            CreatePasswordState.FillUsername(
                username
            )
        )
    }

    fun savePassword(password: String) {
        if (!validatePassword(password)) return

        authRepository
            .registerWithEmail(
                email = email,
                password = password,
                confirmPassword = password,
                firstName = "",
                lastName = ""
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(CreatePasswordState.ShowLoading)
            }
            .doOnSuccess {
                _state.onNext(CreatePasswordState.HideLoading)
            }
            .doOnError {
                _state.onNext(CreatePasswordState.HideLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state
                        .onNext(
                            CreatePasswordState.StartVerificationCodeScreen(
                                email,
                                phone
                            )
                        )
                },
                onError = { throwable ->
                    Timber.e(throwable)
                    _state
                        .onNext(
                            CreatePasswordState.Error(
                                throwable.getThrowableError()
                            )
                        )
                }
            )
            .addTo(disposables)
    }

    fun onPasswordTextChange(password: String) {
        if (validatePasswordNotEmpty(password)) {
            _state
                .onNext(
                    CreatePasswordState.EnableButton
                )
        } else {
            _state
                .onNext(
                    CreatePasswordState.DisableButton
                )
        }
    }

    private fun validatePassword(password: String): Boolean {
        if (!validatePasswordNotEmpty(password)) return false

        if (password.length < PASSWORD_MIN_LENGTH) {
            _state
                .onNext(
                    CreatePasswordState.PasswordBelowMinLength
                )
            return false
        }

        if (password.length > PASSWORD_MAX_LENGTH) {
            _state
                .onNext(
                    CreatePasswordState.PasswordExceedsMaxLength
                )
            return false
        }

        return true
    }

    private fun validatePasswordNotEmpty(password: String): Boolean {
        return password.isNotEmpty()
    }
}
