package com.appetiser.moviesearch.features.auth.login

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.MainGraphDirections
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentEmailLoginBinding
import com.appetiser.moviesearch.ext.disabledWithAlpha
import com.appetiser.moviesearch.ext.enabledWithAlpha
import com.appetiser.moviesearch.ext.getThrowableError
import com.appetiser.moviesearch.ext.setCustomPasswordTransformation
import com.appetiser.moviesearch.features.main.MainActivity
import com.appetiser.moviesearch.utils.NavigationUtils
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class LoginFragment : BaseViewModelFragment<FragmentEmailLoginBinding, LoginViewModel>() {

    companion object {
        private const val KEY_PASSWORD = "password"
    }

    private val args by navArgs<LoginFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_email_login

    override fun canBack(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViews(savedInstanceState)
        observeInputViews()
        setUpViewModels()
        setupToolbar()

        viewModel.setUsername(
            args.email,
            args.phone
        )
    }

    private fun setUpViews(savedInstanceState: Bundle?) {
        binding.inputLayoutPassword.setCustomPasswordTransformation()
        binding.inputPassword.apply {
            setText(savedInstanceState?.getString(KEY_PASSWORD))

            editorActionEvents()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            viewModel.login(
                                binding.txtUsername.text.toString(),
                                binding.inputPassword.text.toString()
                            )
                        }
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }

        binding
            .btnContinue
            .ninjaTap {
                viewModel
                    .login(
                        binding.txtUsername.text.toString(),
                        binding.inputPassword.text.toString()
                    )
            }
            .addTo(disposables)

        binding
            .forgotPassword
            .ninjaTap {
                navigate(
                    LoginFragmentDirections.actionLoginFragmentToForgotPasswordFragment(
                        binding.txtUsername.text.toString()
                    )
                )
            }
            .addTo(disposables)
    }

    private fun observeInputViews() {
        val passwordObservable = binding.inputPassword.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() && it.length >= 8 }

        passwordObservable
            .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    if (it) {
                        binding.btnContinue.enabledWithAlpha()
                    } else {
                        binding.btnContinue.disabledWithAlpha()
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun setUpViewModels() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: LoginState) {
        when (state) {
            is LoginState.GetUsername -> {
                binding.txtUsername.text = state.username
            }
            is LoginState.NoFullName -> {
                navigateToOnBoardingFlow()
            }
            is LoginState.NoProfilePhoto -> {
                navigateToUploadProfilePhotoScreen()
            }
            is LoginState.LoginSuccess -> {
                navigateToHomeScreen()
            }
            is LoginState.UserNotVerified -> {
                navigateToVerificationCodeScreen(state)
            }
            is LoginState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.throwable.getThrowableError()
                )
            }
            is LoginState.ShowProgressLoading -> {
                binding.loading setVisible true
            }

            is LoginState.HideProgressLoading -> {
                binding.loading setVisible false
            }
        }
    }

    private fun navigateToHomeScreen() {
        navigate(
            MainGraphDirections
                .actionGlobalToHomeGraph()
        )
    }

    private fun navigateToVerificationCodeScreen(state: LoginState.UserNotVerified) {
        NavigationUtils.navigateToRegisterVerificationCodeScreen(
            mainActivity = requireActivity() as MainActivity,
            email = state.email,
            phone = state.phone
        )
    }

    private fun navigateToUploadProfilePhotoScreen() {
        NavigationUtils.navigateToUploadProfilePhotoScreen(
            mainActivity = requireActivity() as MainActivity,
            hideBackButton = true,
            isExitOnBack = false
        )
    }

    private fun navigateToOnBoardingFlow() {
        navigate(
            LoginFragmentDirections.actionLoginFragmentToOnboardingGraph()
        )
    }
}
