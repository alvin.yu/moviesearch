package com.appetiser.moviesearch.features.auth.register.email

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentAddEmailBinding
import com.appetiser.moviesearch.features.auth.register.profile.UploadProfilePhotoFragmentArgs
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class AddEmailFragment : BaseViewModelFragment<FragmentAddEmailBinding, AddEmailViewModel>() {

    private val args: UploadProfilePhotoFragmentArgs by navArgs()

    override fun getLayoutId(): Int = R.layout.fragment_add_email

    override fun canBack() = true

    override fun isExitOnBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViews()
        setupVmObservers()
        setupToolbar()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun setupToolbar() {
        if (!args.hideBackButton) {
            enableToolbarHomeIndicator(binding.toolbar.toolbarView)
        }
    }

    private fun setUpViews() {
        binding.inputEmail.apply {
            editorActionEvents()
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            if (binding.btnContinue.isEnabled) {
                                changeEmail()
                            }
                        }
                    }
                )
                .addTo(disposables)

            textChanges()
                .skipInitialValue()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeBy(
                    onNext = { text ->
                        viewModel.onEmailTextChanged(text.toString())
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }

        binding
            .btnContinue
            .ninjaTap {
                changeEmail()
            }
            .addTo(disposables)

        binding.btnSkip
            .ninjaTap {
                navigate(
                    AddEmailFragmentDirections.actionAddEmailFragmentToInputNameFragment()
                )
            }
            .addTo(disposables)
    }

    private fun changeEmail() {
        binding.inputLayoutEmail.error = ""
        binding.root.hideKeyboard()
        viewModel
            .updateEmail(
                binding.inputEmail.text.toString().trim()
            )
    }

    private fun handleState(state: AddEmailState) {
        when (state) {
            AddEmailState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            AddEmailState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            AddEmailState.InvalidEmail -> {
                binding
                    .inputLayoutEmail
                    .error = getString(R.string.invalid_email)
            }
            AddEmailState.ShowLoading -> {
                binding.loading setVisible true
            }
            AddEmailState.HideLoading -> {
                binding.loading setVisible false
            }
            is AddEmailState.EmailAlreadyTaken -> {
                binding
                    .inputLayoutEmail
                    .error = state.message
            }
            is AddEmailState.NavigateToSubscriptionScreen -> {
                navigate(
                    AddEmailFragmentDirections.actionAddEmailFragmentToSubscriptionFragment()
                )
            }
            is AddEmailState.NavigateToUploadPhotoScreen -> {
                navigate(
                    AddEmailFragmentDirections.actionAddEmailFragmentToUploadProfilePhotoFragment(hideBackButton = false, isExitOnBack = false)
                )
            }

            is AddEmailState.NavigateToInputNameScreen -> {
                navigate(
                    AddEmailFragmentDirections.actionAddEmailFragmentToInputNameFragment()
                )
            }

            is AddEmailState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }
}
