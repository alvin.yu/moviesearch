package com.appetiser.moviesearch.features.account.changephone.newphone

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentChangePhoneNewPhoneBinding
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangePhoneNewPhoneFragment : BaseViewModelFragment<FragmentChangePhoneNewPhoneBinding, ChangePhoneNewPhoneViewModel>() {

    private val args by navArgs<ChangePhoneNewPhoneFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_change_phone_new_phone

    override fun canBack(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setVerificationToken(
            args.verificationToken
        )

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.loadAccount()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangePhoneNewPhoneState) {
        when (state) {
            ChangePhoneNewPhoneState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangePhoneNewPhoneState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangePhoneNewPhoneState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangePhoneNewPhoneState.HideLoading -> {
                binding.loading setVisible false
            }
            is ChangePhoneNewPhoneState.PhoneNumberError -> {
                binding
                    .inputPhone
                    .error = state.message
            }
            is ChangePhoneNewPhoneState.ChangePhoneSuccess -> {
                navigate(
                    ChangePhoneNewPhoneFragmentDirections.actionChangePhoneNewPhoneFragmentToChangePhoneVerifyCodeFragment(
                        state.phoneNumber,
                        state.verificationToken
                    )
                )
            }

            is ChangePhoneNewPhoneState.UpdatedPhoneNumber -> {
                navigate(
                    ChangePhoneNewPhoneFragmentDirections.actionChangePhoneNewPhoneFragmentToAccountSettingsGraph()
                )
            }

            is ChangePhoneNewPhoneState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputPhone
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel
                    .onPhoneTextChanged(it.toString())
            }
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPhone.error = ""
                binding.root.hideKeyboard()

                val countryCode = binding.countryCodePicker.selectedCountryCode
                val countryNameCode = binding.countryCodePicker.selectedCountryNameCode
                val phoneNumber = binding.inputPhone.text.toString()

                viewModel
                    .changePhone(
                        countryCode,
                        countryNameCode,
                        phoneNumber
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }
}
