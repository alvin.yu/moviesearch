package com.appetiser.moviesearch.features.home

import com.appetiser.module.domain.models.movie.Movie

sealed class MovieListState {

    object ShowLoading : MovieListState()

    object HideLoading : MovieListState()

    data class DisplayMovieList(val movieList: List<Movie>) : MovieListState()

    data class DisplayMovie(val movie: Movie) : MovieListState()

    data class DisplayLastUserVisit(val formattedDate: String) : MovieListState()
}
