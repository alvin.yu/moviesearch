package com.appetiser.moviesearch.features.auth.register.email

sealed class AddEmailState {

    object EnableButton : AddEmailState()

    object DisableButton : AddEmailState()

    object ShowLoading : AddEmailState()

    object HideLoading : AddEmailState()

    object InvalidEmail : AddEmailState()

    object NavigateToInputNameScreen : AddEmailState()

    object NavigateToUploadPhotoScreen : AddEmailState()

    object NavigateToSubscriptionScreen : AddEmailState()

    data class EmailAlreadyTaken(val message: String) : AddEmailState()

    data class Error(val message: String) : AddEmailState()
}
