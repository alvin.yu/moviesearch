package com.appetiser.moviesearch.features.account.changepassword.newpassword

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.extensions.*
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelFragment
import com.appetiser.moviesearch.databinding.FragmentChangeNewPasswordBinding
import com.appetiser.moviesearch.ext.setCustomPasswordTransformation
import com.appetiser.moviesearch.utils.ViewUtils
import com.jakewharton.rxbinding3.widget.editorActionEvents
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeNewPasswordFragment : BaseViewModelFragment<FragmentChangeNewPasswordBinding, ChangeNewPasswordViewModel>() {

    private val args by navArgs<ChangeNewPasswordFragmentArgs>()

    override fun getLayoutId(): Int = R.layout.fragment_change_new_password

    override fun canBack() = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setOldPasswordAndToken(
            args.oldPassword,
            args.verificationToken
        )

        setupViews()
        setupToolbar()
        setupViewModels()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbar.toolbarView)
    }

    private fun setupViews() {
        binding.passwordMaxLength = PASSWORD_MAX_LENGTH
        binding.inputLayoutPassword.setCustomPasswordTransformation()
        binding.inputPassword.apply {
            this.textChanges()
                .skipInitialValue()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = { text ->
                        viewModel.onPasswordTextChanged(text.toString())
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)

            this.editorActionEvents()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it.actionId == EditorInfo.IME_ACTION_GO) {
                            binding.inputLayoutPassword.error = ""
                            binding.root.hideKeyboard()
                            viewModel
                                .changePassword(
                                    binding.inputPassword.text.toString().trim()
                                )
                        }
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)
        }

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPassword.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .changePassword(
                        binding.inputPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupViewModels() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }

    private fun handleState(state: ChangeNewPasswordState) {
        when (state) {
            ChangeNewPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangeNewPasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangeNewPasswordState.ShowProgressLoading -> {
                binding.loading setVisible true
            }
            ChangeNewPasswordState.HideProgressLoading -> {
                binding.loading setVisible false
            }
            ChangeNewPasswordState.InvalidPassword -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.invalid_password)
            }
            is ChangeNewPasswordState.Success -> {
                navigate(
                    ChangeNewPasswordFragmentDirections.actionChangeNewPasswordFragmentToAccountSettingsGraph()
                )
            }
            is ChangeNewPasswordState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }
}
