package com.appetiser.moviesearch.features.splash

import android.os.Bundle
import androidx.core.os.bundleOf
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelActivity
import com.appetiser.moviesearch.databinding.ActivitySplashBinding
import com.appetiser.moviesearch.features.main.MainActivity
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class SplashActivity : BaseViewModelActivity<ActivitySplashBinding, SplashViewModel>() {
    override fun getLayoutId(): Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: SplashState) {
        when (state) {
            SplashState.UserIsLoggedIn -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(MainActivity.EXTRA_START_SCREEN to MainActivity.START_MAIN)
                )
            }
            SplashState.UserIsNotLoggedIn -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(MainActivity.EXTRA_START_SCREEN to MainActivity.START_WALKTHROUGH)
                )
            }
            SplashState.NoFullName -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(MainActivity.EXTRA_START_SCREEN to MainActivity.START_ONBOARDING)
                )
            }
            SplashState.NoProfilePhoto -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(MainActivity.EXTRA_START_SCREEN to MainActivity.START_UPLOAD_PROFILE_PHOTO)
                )
            }
        }

        finishAffinity()
    }
}
