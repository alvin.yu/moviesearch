package com.appetiser.moviesearch.features.account.primaryphone

sealed class PrimaryPhoneVerificationState {
    object InitializeChangeEmail : PrimaryPhoneVerificationState()

    object InitializeChangePhoneNumber : PrimaryPhoneVerificationState()

    object InitializeDeleteAccount : PrimaryPhoneVerificationState()

    object HideKeyboard : PrimaryPhoneVerificationState()

    object ShowLoading : PrimaryPhoneVerificationState()

    object HideLoading : PrimaryPhoneVerificationState()

    object ResendCodeSuccess : PrimaryPhoneVerificationState()

    data class PhoneVerificationSuccess(val verificationToken: String) : PrimaryPhoneVerificationState()

    data class EmailVerificationSuccess(val verificationToken: String) : PrimaryPhoneVerificationState()

    object NavigateToWalkthroughScreen : PrimaryPhoneVerificationState()

    data class DisplayPhone(val phoneNumber: String) : PrimaryPhoneVerificationState()

    data class InvalidVerificationCode(val message: String) : PrimaryPhoneVerificationState()

    data class Error(val message: String) : PrimaryPhoneVerificationState()

    data class ResendTimer(val time: Long) : PrimaryPhoneVerificationState()

    object ResendTimerCompleted : PrimaryPhoneVerificationState()

    object ShowResendProgressLoading : PrimaryPhoneVerificationState()

    object HideResendProgressLoading : PrimaryPhoneVerificationState()
}
