package com.appetiser.moviesearch.features.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.ColorInt
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.appetiser.baseplate.features.main.MainState
import com.appetiser.module.common.extensions.makeStatusBarNonTransparent
import com.appetiser.module.common.extensions.makeStatusBarTransparent
import com.appetiser.moviesearch.MainGraphDirections
import com.appetiser.moviesearch.R
import com.appetiser.moviesearch.base.BaseViewModelActivity
import com.appetiser.moviesearch.databinding.ActivityMainBinding
import com.appetiser.moviesearch.ext.getThemeColor
import com.appetiser.moviesearch.ext.makeSoftInputModeAdjustPan
import com.appetiser.moviesearch.ext.makeSoftInputModeAdjustResize
import com.appetiser.moviesearch.utils.NavigationUtils
import com.appetiser.moviesearch.utils.ViewUtils
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MainActivity : BaseViewModelActivity<ActivityMainBinding, MainViewModel>() {
    companion object {
        const val EXTRA_START_SCREEN = "EXTRA_START_SCREEN"
        const val START_MAIN = "START_MAIN"
        const val START_WALKTHROUGH = "START_WALKTHROUGH"
        const val START_ONBOARDING = "START_ONBOARDING"
        const val START_UPLOAD_PROFILE_PHOTO = "START_UPLOAD_PROFILE_PHOTO"

        fun openActivity(context: Context, extras: Bundle) {
            context.startActivity(
                Intent(
                    context,
                    MainActivity::class.java
                ).apply {
                    putExtras(extras)
                }
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handleExtras()

        setupVmObservers()
        setupViews()
    }

    override fun onResume() {
        super.onResume()
        viewModel.versionCheck()
    }

    private fun setupViews() {
        navController().addOnDestinationChangedListener { _, destination, _ ->
            viewModel.onDestinationChanged(destination.id)
        }
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: MainState) {
        when (state) {
            MainState.ChangeStatusBarToDarkTransparent -> {
                makeStatusBarTransparent(false)
            }
            MainState.ChangeStatusBarToLightTransparent -> {
                makeStatusBarTransparent(true)
            }
            MainState.ChangeStatusBarToDefault -> {
                makeStatusBarNonTransparent()
                changeStatusBarColor(getThemeColor(android.R.attr.colorBackground))
            }
            MainState.ChangeSoftInputModeAdjustPan -> {
                makeSoftInputModeAdjustPan()
            }
            MainState.ChangeSoftInputModeDefault -> {
                makeSoftInputModeAdjustResize()
            }
            is MainState.ShowUpdateGateForced -> {
                ViewUtils.showUpdateGate(
                    this,
                    state.storeUrl,
                    true
                )
            }
            is MainState.ShowUpdateGateRecommended -> {
                ViewUtils.showUpdateGate(
                    this,
                    state.storeUrl,
                    false,
                    updateLaterClickListener = {
                        viewModel.onUpdateLaterClicked()
                    }
                )
            }
        }
    }

    private fun changeStatusBarColor(@ColorInt color: Int) {
        if (window.statusBarColor != color) {
            window.statusBarColor = color
        }
    }

    private fun handleExtras() {
        intent
            ?.extras
            ?.let { bundle ->
                val startScreen = bundle.getString(EXTRA_START_SCREEN) ?: return

                require(startScreen.isNotEmpty()) {
                    "EXTRA_START_SCREEN not found!"
                }

                when (startScreen) {
                    START_MAIN -> {
                        navigate(
                            MainGraphDirections
                                .actionGlobalToHomeGraph(),
                            R.id.homeFragment
                        )
                    }
                    START_ONBOARDING -> {
                        navigate(
                            MainGraphDirections
                                .actionGlobalToOnboardingGraph(),
                            R.id.inputNameFragment
                        )
                    }
                    START_WALKTHROUGH -> {
                        navigate(
                            MainGraphDirections
                                .actionGlobalToWalkthroughGraph(),
                            R.id.walkthroughFragment
                        )
                    }
                    START_UPLOAD_PROFILE_PHOTO -> {
                        NavigationUtils.navigateToUploadProfilePhotoScreen(
                            mainActivity = this,
                            hideBackButton = true,
                            isExitOnBack = true
                        )
                    }
                }
            }
    }

    private fun navigate(navDirections: NavDirections, destinationScreenId: Int) {
        if (navController().currentDestination!!.id != destinationScreenId) {
            navController()
                .navigate(navDirections)
        }
    }

    fun navController() = findNavController(R.id.nav_host_fragment)
}
