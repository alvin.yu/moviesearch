package com.appetiser.moviesearch.features.account.primaryemail

enum class PrimaryEmailPasswordScreenFlow(val value: Int) {
    CHANGE_PHONE(0), CHANGE_EMAIL(1), CHANGE_PASSWORD(2), CHANGE_DELETE(3)
}
