package com.appetiser.moviesearch.features.auth.register.profile

sealed class UploadPhotoState {

    object ShowProgressLoading : UploadPhotoState()

    object HideProgressLoading : UploadPhotoState()

    object ShowSubscriptionScreen : UploadPhotoState()

    data class ErrorUploadPhoto(val throwable: Throwable) : UploadPhotoState()
}
