package com.appetiser.moviesearch.components

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.appetiser.moviesearch.R
import com.google.android.material.textview.MaterialTextView

/**
 * Component used for navigation purposes. Contains [leftIcon], [rightIcon], [title], and [description].
 */
class CompoundNavigationButton @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attributeSet, defStyle) {

    private lateinit var container: ConstraintLayout
    private lateinit var imgLeftIcon: AppCompatImageView
    private lateinit var imgRightIcon: AppCompatImageView
    private lateinit var txtTitle: MaterialTextView
    private lateinit var txtDescription: MaterialTextView

    private var title: String = ""
    private var description: String = ""
    private var leftIcon: Drawable? = null
    private var rightIcon: Drawable? = null

    init {
        val attrs = context.obtainStyledAttributes(attributeSet, R.styleable.CompoundNavigationButton)

        try {
            title =
                attrs.getString(
                    R.styleable.CompoundNavigationButton_navigationButtonTitle
                ) ?: ""
            description =
                attrs.getString(
                    R.styleable.CompoundNavigationButton_navigationButtonDescription
                ) ?: ""
            leftIcon =
                attrs.getDrawable(
                    R.styleable.CompoundNavigationButton_navigationButtonLeftIcon
                )
            rightIcon =
                attrs.getDrawable(
                    R.styleable.CompoundNavigationButton_navigationButtonRightIcon
                )
        } finally {
            attrs.recycle()
        }
    }

    private fun initViews() {
        View.inflate(context, R.layout.component_compound_navigation_button, this)

        container = findViewById(R.id.container)
        imgLeftIcon = findViewById(R.id.imgLeftIcon)
        imgRightIcon = findViewById(R.id.imgRightIcon)
        txtTitle = findViewById(R.id.txtTitle)
        txtDescription = findViewById(R.id.txtDescription)

        imgLeftIcon.setImageDrawable(leftIcon)
        imgRightIcon.setImageDrawable(rightIcon)
        txtTitle.text = title
        txtDescription.text = description
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        initViews()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
    }

    override fun setOnClickListener(l: OnClickListener?) {
        container.setOnClickListener(l)
    }
}
