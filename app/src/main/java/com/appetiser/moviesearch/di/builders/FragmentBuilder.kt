package com.appetiser.moviesearch.di.builders

import com.appetiser.moviesearch.di.scopes.FragmentScope
import com.appetiser.moviesearch.features.account.AccountFragment
import com.appetiser.moviesearch.features.account.about.AboutFragment
import com.appetiser.moviesearch.features.account.about.WebViewFragment
import com.appetiser.moviesearch.features.account.changeemail.newemail.ChangeEmailNewEmailFragment
import com.appetiser.moviesearch.features.account.changeemail.verifycode.ChangeEmailVerifyCodeFragment
import com.appetiser.moviesearch.features.account.changepassword.currentpassword.ChangeCurrentPasswordFragment
import com.appetiser.moviesearch.features.account.changepassword.newpassword.ChangeNewPasswordFragment
import com.appetiser.moviesearch.features.account.changephone.newphone.ChangePhoneNewPhoneFragment
import com.appetiser.moviesearch.features.account.changephone.verifycode.ChangePhoneVerifyCodeFragment
import com.appetiser.moviesearch.features.account.primaryemail.PrimaryEmailPasswordFragment
import com.appetiser.moviesearch.features.account.primaryphone.PrimaryPhoneVerificationFragment
import com.appetiser.moviesearch.features.account.settings.AccountSettingsFragment
import com.appetiser.moviesearch.features.auth.forgotpassword.ForgotPasswordFragment
import com.appetiser.moviesearch.features.auth.forgotpassword.newpassword.NewPasswordFragment
import com.appetiser.moviesearch.features.auth.forgotpassword.verification.ForgotPasswordVerificationFragment
import com.appetiser.moviesearch.features.auth.landing.LandingFragment
import com.appetiser.moviesearch.features.auth.login.LoginFragment
import com.appetiser.moviesearch.features.auth.register.createpassword.CreatePasswordFragment
import com.appetiser.moviesearch.features.auth.register.details.InputNameFragment
import com.appetiser.moviesearch.features.auth.register.email.AddEmailFragment
import com.appetiser.moviesearch.features.auth.register.profile.UploadProfilePhotoFragment
import com.appetiser.moviesearch.features.auth.verification.VerificationFragment
import com.appetiser.moviesearch.features.auth.subscription.SubscriptionFragment
import com.appetiser.moviesearch.features.auth.walkthrough.WalkthroughFragment
import com.appetiser.moviesearch.features.auth.welcome.WelcomeFragment
import com.appetiser.moviesearch.features.home.HomeFragment
import com.appetiser.moviesearch.features.profile.editprofile.EditProfileFragment
import com.appetiser.moviesearch.features.home.MovieDetailFragment
import com.appetiser.moviesearch.features.home.SelectAMovieFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeFeedsFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeAccountFragment(): AccountFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeWalkthroughFragment(): WalkthroughFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeInputNameFragment(): InputNameFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeAddEmailFragment(): AddEmailFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeUploadProfilePhotoFragment(): UploadProfilePhotoFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSubscriptionFragment(): SubscriptionFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordFragment(): ForgotPasswordFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeRegisterVerificationFragment(): VerificationFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordVerificationFragment(): ForgotPasswordVerificationFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeNewPasswordFragment(): NewPasswordFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeLandingFragment(): LandingFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeWelcomeFragment(): WelcomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeCreatePasswordFragment(): CreatePasswordFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeEditProfileFragment(): EditProfileFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeAboutFragment(): AboutFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeWebViewFragment(): WebViewFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeAccountSettingsFragment(): AccountSettingsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailNewEmailFragment(): ChangeEmailNewEmailFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailVerifyCodeFragment(): ChangeEmailVerifyCodeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeChangeCurrentPasswordFragment(): ChangeCurrentPasswordFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeChangeNewPasswordFragment(): ChangeNewPasswordFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeChangePhoneNewPhoneFragment(): ChangePhoneNewPhoneFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeChangePhoneVerifyCodeFragment(): ChangePhoneVerifyCodeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributePrimaryPhoneVerificationFragment(): PrimaryPhoneVerificationFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributePrimaryEmailPasswordFragment(): PrimaryEmailPasswordFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMovieDetailFragment(): MovieDetailFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSelectAMovieFragment(): SelectAMovieFragment
}
