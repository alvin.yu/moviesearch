package com.appetiser.moviesearch.di.builders

import com.appetiser.moviesearch.di.scopes.ActivityScope
import com.appetiser.moviesearch.features.main.MainActivity
import com.appetiser.moviesearch.features.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
