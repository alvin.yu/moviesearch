package com.appetiser.moviesearch.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
