package com.appetiser.module.data.features.movie

import com.appetiser.module.domain.models.movie.Movie
import com.appetiser.module.local.features.movie.MovieLocalSource
import com.appetiser.module.network.features.movie.MovieRemoteSource
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

private const val TERM_STAR = "star"
private const val COUNTRY_AU = "au"
private const val MEDIA_MOVIE = "movie"

private const val LIMIT_200 = "20"

class MovieRepositoryImpl @Inject constructor(
    private val movieLocalSource: MovieLocalSource,
    private val movieRemoteSource: MovieRemoteSource
) : MovieRepository {

    override fun getMovie(trackId: Long): Single<Movie> {
        return movieLocalSource.getMovieById(trackId)
    }

    override fun getMovieList(): Single<List<Movie>> {
        return movieRemoteSource.getMovies(TERM_STAR, COUNTRY_AU, MEDIA_MOVIE, LIMIT_200)
    }

    override fun getCachedMovieList(): Flowable<List<Movie>> {
        return movieLocalSource.getAllMovies()
    }

    override fun saveMovieList(movieList: List<Movie>): Single<List<Long>> {
        return movieLocalSource.saveMovieList(movieList)
    }

    override fun getLastUserVisit(): Single<Long> {
        return movieLocalSource.getLastUserVisit()
    }
}
