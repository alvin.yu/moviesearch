package com.appetiser.module.data.features.session

import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.local.features.session.SessionLocalSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class SessionRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource
) : SessionRepository {
    override fun getSession(): Single<Session> {
        return sessionLocalSource
            .getSession()
    }

    override fun saveSession(session: Session): Single<Session> {
        return sessionLocalSource.saveSession(session)
    }

    override fun clearSession(): Completable {
        return sessionLocalSource.clearSession()
    }
}
