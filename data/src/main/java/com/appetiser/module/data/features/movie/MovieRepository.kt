package com.appetiser.module.data.features.movie

import com.appetiser.module.domain.models.movie.Movie
import io.reactivex.Flowable
import io.reactivex.Single

interface MovieRepository {

    fun getMovie(trackId: Long): Single<Movie>

    fun getMovieList(): Single<List<Movie>>

    fun getCachedMovieList(): Flowable<List<Movie>>

    fun saveMovieList(movieList: List<Movie>): Single<List<Long>>

    fun getLastUserVisit(): Single<Long>
}
