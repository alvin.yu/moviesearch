package com.appetiser.module.data.features

import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.domain.models.user.User

object Stubs {
    val email = "foo@bar.baz"
    val password = "password"
    val passwordConfirmation = "password"
    val firstName = "foo"
    val lastName = "bar"
    val mobileNumber = "09177707257"

    val USER_SESSION_LOGGED_IN = User(
        id = "1231232132123",
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "12/28/1993"
    )

    val USER_SESSION_LOGGED_IN_NOT_ONBOARDED = User(
        id = "1231232132123",
        fullName = "",
        firstName = "",
        lastName = "",
        email = "",
        avatarPermanentThumbUrl = "/",
        avatarPermanentUrl = "",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "12/28/1993"
    )

    val USER_NOT_VERIFIED = User(
        id = "1111",
        fullName = "Test",
        firstName = "Test",
        lastName = "Test",
        email = "test@test.test",
        avatarPermanentThumbUrl = "http://photo",
        avatarPermanentUrl = "http://photo",
        verified = false,
        emailVerified = false,
        phoneNumber = "09177707257",
        phoneNumberVerified = false,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "12/28/1993"
    )

    private val validAccessToken = AccessToken(token = "sampleToken")

    val SESSION_LOGGED_IN = Session(USER_SESSION_LOGGED_IN, validAccessToken)

    val PAIR_USER_SESSION_LOGGED_IN = Pair(USER_SESSION_LOGGED_IN, validAccessToken)

    val PAIR_USER_NOT_VERIFIED = Pair(USER_NOT_VERIFIED, validAccessToken)

    val PAIR_USER_SESSION_LOGGED_IN_NOT_ONBOARDED = Pair(USER_SESSION_LOGGED_IN_NOT_ONBOARDED, validAccessToken)

    val USER_SESSION_NOT_LOGGED_IN = User.empty()
}
