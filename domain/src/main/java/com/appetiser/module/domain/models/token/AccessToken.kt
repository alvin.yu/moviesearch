package com.appetiser.module.domain.models.token

data class AccessToken(
    val token: String = "",
    val refresh: String = "",
    val tokenType: String = "",
    val expiresIn: String = ""
) {

    val bearerToken get() = "Bearer $token"
}
