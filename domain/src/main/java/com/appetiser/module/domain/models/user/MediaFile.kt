package com.appetiser.module.domain.models.user

data class MediaFile(
    val id: String,
    val name: String,
    val fileName: String,
    val collectionName: String,
    val mimeType: String,
    val size: Long,
    val createdAt: String,
    val url: String,
    val thumbUrl: String
) {
    companion object {
        fun empty(): MediaFile {
            return MediaFile(
                id = "",
                name = "",
                fileName = "",
                collectionName = "",
                mimeType = "",
                size = 0,
                createdAt = "",
                url = "",
                thumbUrl = ""
            )
        }
    }

    /**
     * Returns true if media file is empty.
     */
    fun isEmpty(): Boolean {
        return id.isEmpty()
    }

    /**
     * Returns true if media file is not empty.
     */
    fun isNotEmpty(): Boolean {
        return !isEmpty()
    }
}
