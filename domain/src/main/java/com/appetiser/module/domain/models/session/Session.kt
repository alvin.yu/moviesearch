package com.appetiser.module.domain.models.session

import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User

data class Session(
    var user: User,
    var accessToken: AccessToken,
    var updateGate: UpdateGate? = null,
    var recommendUpdateTimeStamp: Long = 0L,
    var hasSkippedEmailVerification: Boolean = false
)