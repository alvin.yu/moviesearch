package com.appetiser.module.domain.models

class CountryCode(
    val id: Long = 0,
    val name: String = "",
    val callingCode: String = "",
    val flag: String = ""
)
