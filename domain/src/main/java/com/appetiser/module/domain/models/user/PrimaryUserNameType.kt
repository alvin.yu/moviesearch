package com.appetiser.module.domain.models.user

enum class PrimaryUserNameType(val value: String) {
    EMAIL("email"), PHONE_NUMBER("phone_number")
}
