package com.appetiser.module.domain.models.movie

data class Movie(
    val trackId         : Long  ,
    val trackName       : String,
    val artworkUrl30    : String,
    val artworkUrl100   : String,
    val trackPrice      : Double,
    val primaryGenreName: String,
    val longDescription : String,
) {
    companion object {
        fun empty(): Movie {
            return Movie(
                trackId          = 0L,
                trackName        = "",
                artworkUrl30     = "",
                artworkUrl100    = "",
                trackPrice       = 0.0,
                primaryGenreName = "",
                longDescription  = "",
            )
        }
    }
}