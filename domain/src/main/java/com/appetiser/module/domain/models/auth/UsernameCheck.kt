package com.appetiser.module.domain.models.auth

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class UsernameCheck(
    val userName: String,
    val userNameType: UsernameType,
    val authType: AuthType,
    val hasPassword: Boolean
)

sealed class AuthType(val value: String) : Parcelable {
    @Parcelize
    object Otp : AuthType("otp")

    @Parcelize
    object Password : AuthType("password")

    companion object {
        fun getAuthTypeFromString(authTypeString: String): AuthType {
            return when (authTypeString) {
                AuthType.Otp.value -> AuthType.Otp
                AuthType.Password.value -> AuthType.Password
                else -> {
                    throw IllegalArgumentException("Unknown auth type: $authTypeString!")
                }
            }
        }
    }
}

sealed class UsernameType(val value: String) : Parcelable {
    @Parcelize
    object Email : UsernameType("email")

    @Parcelize
    object Phone : UsernameType("phone_number")

    companion object {
        fun getUsernameTypeFromString(usernameTypeString: String): UsernameType {
            return when (usernameTypeString) {
                Email.value -> Email
                Phone.value -> Phone
                else -> {
                    throw IllegalArgumentException("Unknown username type: $usernameTypeString!")
                }
            }
        }
    }
}
