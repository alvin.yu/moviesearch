package com.appetiser.module.common.extensions

import android.widget.EditText
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import java.util.*

fun EditText.capitalizeFirstLetter(): Disposable {
    return this.textChangeEvents()
        .skipInitialValue()
        .observeOn(AndroidSchedulers.mainThread())
        .map {
            val typeString = it.text.toString()
            if (typeString.length == 1) {
                if (typeString.single().isLowerCase()) {
                    setText(typeString.toUpperCase(Locale.getDefault()))
                }
            }
            return@map typeString
        }
        .subscribeBy(
            onNext = {
                setSelection(it.length)
            }
        )
}
